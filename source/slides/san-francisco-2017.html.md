---
title: San Francisco 2017
---

![](/assets/images/san-francisco/golden-gate-from-coit-tower.jpg)

---

I was in San Francisco from Oct. 18–25.

I went to SF to attend Accessibility Camp Bay Area and to meet with people in tech who have an interest in tech writing and documentation.

---

## People I met

---

### Alaina Kafkes

developer at [Medium](https://medium.com/)
{: .center}

---

Alaina is a junior developer who has written articles on tech writing for the developer website [dev.to](https://dev.to/).

She writes and blogs frequently, and often writes about the problems she encounters during work and how she goes about solving them.

---

#### Takeaways

- Medium has no dedicated documentation team

- Writing about the issues we encounter in our work and as a team could be a good way to reflect

- If we ever get time, maybe we should have a (voluntary) team blog where we publish ideas and explain our thought processes

---

![](/assets/images/san-francisco/pier-39-sea-lions.jpg)

Sea lions at Pier 39
{: .center}

---

### Slack

![](/assets/images/san-francisco/slack.jpg)

---

I met with some of [Slack](https://slack.com/)'s help content writers at one of the company's offices downtown.

We hung out, had coffee, and chatted about tech writing stuff.

---

#### Takeaways

- Slack uses a customized _ZenDesk Help Center_
- They were pretty surprised by our developer-like workflow (using the terminal, git, and text editors)
- They had localized help content before Slack (the product) was localized
- Every quarter, the team does a review of their help articles with the worst helpfulness ratings and spends time rewriting or fixing them

---

![](/assets/images/san-francisco/lombard-street.jpg)

Lombard Street
{: .center}

---

### Tom Johnson

technical writer, [Amazon Lab126](https://www.lab126.com/)
{: .center}

~~

![](/assets/images/san-francisco/tom-johnson.jpg)

---

I had lunch with Tom Johnson in Sunnyvale, about an hour south of San Francisco.

Tom's a prolific blogger and runs the popular tech writing blog [idratherbewriting.com](http://idratherbewriting.com).

---

#### Takeaways

- Localizing Jekyll sites is very difficult, and there is no easy way to do it
- Tom writes for his blog in the evenings, but also sometimes takes a half hour in the morning to do non-work-related writing

---

![](/assets/images/san-francisco/alamo-square-park.jpg)

Alamo Square Park
{: .center}

---

### Tom MacWright

developer, [Observable](https://observablehq.com/)
{: .center}

~~

![](/assets/images/san-francisco/tom-macwright.jpg)

---

Tom used to work at Mapbox, and he has led development on [OpenStreetMap](https://www.openstreetmap.org/)'s map editor.

Tom helped work on a library called *retext-mapbox-standard*, a content linting library just like Rorybot.

---

#### Takeaways

- Tom sometimes practices "unshipping" to deal with burnout
- Mapbox has no dedicated documentation team — developers were expected to write their own docs
- Tom pointed me to a JavaScript library called d3.js that has succeeded greatly despite having mostly example-based documentation

---

### Abbey Blanchard

help content writer, [Airbnb](https://www.airbnb.com)
{: .center}

~~

![](/assets/images/san-francisco/abbey-blanchard.jpg)

~~

![](/assets/images/san-francisco/michael-sui.jpg)

---

I met Abbey at one of Airbnb's offices for coffee and a chat.

She's the only dedicated content writer for [Airbnb help](https://www.airbnb.com/help).

---

#### Takeaways

> Be seen as a service, not a roadblock.

- Abbey suggested it's a good idea to have different "issue queues" for different things: bugs, feature requests, internal issues, and so on
- She's interested in finding better metrics to figure out which of her pages are doing well, as helpfulness rating isn't quite enough

---

### Dan Stevens

technical writer, Atlassian
{: .center}

~~

![](/assets/images/san-francisco/dan-stevens.jpg)

---

Dan works on documentation for Bitbucket, Atlassian's version of git-as-a-service.

He's been a technical writer for a long time and is active in Bay Area tech writing meetups.

---

#### Takeaways

- Bitbucket uses Hugo, a static site generator, for their documentation
- They use a developer workflow just like us, and are bigs fans of it
- Feedback submitted from their "was this page helpful" boxes goes straight into their issues queue

---

![](/assets/images/san-francisco/alcatraz.jpg)

Alcatraz
{: .center}

---

## Accessibility camp

Accessibility (A11y) Camp Bay Area is held in SF in October every year.

It's a free "unconference" that's volunteer-driven and hosted at LinkedIn.

This year about 200 people came out to learn about accessibiity on the web.

---

### Accessibility UX Testing

- knowing that some people use assistive technology (screen readers) is very different from seeing someone use a screen reader to browse your site

- users should be thinking about their goals when they come to your site, not how to navigate the site

---

### Secret Sauce of Successful A11y Squads

> Accessibility is an _and_, not an _or_.

- Training, education, and advocacy can be good first steps in getting buy-in to spend time on a11y

- A11y linters can introduce a11y ideas to people who spend time working with code

---

### A day in the life of a blind user

In this talk, we watched two blind Googlers go about their day using an espresso machine, a pressure cooker, and a wine bottle opener.

- Physical products are often not designed for non-sighted users
- "Smart" home assistants like Google Home can be super helpful to solve these sorts of issues

---

### Designing for people with cognitive limitations

A11y isn't just for blind and deaf users. People with cognitive limitations (like from a traumatic brain injury) can benefit from accessible experiences too.

Part of cognitive a11y is _excellent usability_.

---

Good tech writing is often more usable for people with cognitive limitations, such as:

  - clear language
  - no extraneous information
  - helpful pictures

---

Also, formulae to show reading grade level for content can be useful as a metric, but don't try to rewrite material just to lower the grade level. It's been shown that content rewritten in this way, despite having a lower grade level, becomes _harder to read_.

---

### Biggest takeaways

- We have a unique setup and workflow here, but we share the same problems as other docs teams

- Triaging incoming issues and feature requests is hard and our system could be improved

- A11y is really important and let's talk about it

---
title: About
---

adamhollett.com is the personal website of Adam Hollett, a developer and technical writer in Ottawa, Ontario, Canada.

## Work

[My LinkedIn profile](https://www.linkedin.com/in/adamhollett) has more details on my work history and experience.

### Software development

I'm a Ruby/Rails developer at [Shopify](http://shopify.com), where I work on documentation and support tools to help our merchants. I built a custom Rails engine to transition the [Shopify Help Center](https://help.shopify.com) from a static website to a multi-language dynamic app.

Sometimes I write code to solve small problems or improve the tools I like. I've created a [couple](https://github.com/adamhollett/jekyll-relations) of [gems](https://github.com/adamhollett/writedown).

### Technical writing

As a technical writer, I helped maintain the [Shopify Help Center](https://help.shopify.com) and wrote documentation for features like [Timeline](https://help.shopify.com/manual/productivity-tools/timeline) and [Kit](https://help.shopify.com/manual/promoting-marketing/kit).

I also wrote Ruby code for the upkeep of our documentation platform and improved automated tests for content. With my colleague [Jeremy Hanson-Finger](https://hanson-finger.com), I created [rorybot](https://github.com/Shopify/rorybot), an automated tool for catching style errors in written documentation.

I continue to work with and contribute to documentation for the static site generator [Jekyll](http://jekyllrb.com) and Shopify's template language [Liquid](https://shopify.github.io/liquid).

### Teaching

I returned to [Algonquin College](http://www.algonquincollege.com) for a couple of years to teach the Usability and User Experience course in my former Technical Writing program.

The course covers introductory concepts of UX with a particular focus on usability, and the effects that usability and UX have on the documentation process.

## Education

I have a Postgraduate Diploma in Technical Writing from Algonquin College in Ottawa, Ontario. My main technical writing interests include automation, usability, and accessibility.

I hold a BA in English Language and Literature from Memorial University of Newfoundland. My main literary interests are unreliable narration and magical realism. Some of my favourite books include *Pale Fire*, *Fifth Business*, and *Watership Down*.

While at Memorial, I was involved with the [Grenfell Campus Student Union](http://gcsuonline.ca), the [Memorial English Students' Society](http://www.mun.ca/mess), and the [Writing Centre](http://www.mun.ca/writingcentre/).

## Crosswords

Crossword puzzles I've made have been printed in [The Algonquin Times](http://www.algonquintimes.com) in Ottawa, Ontario, and in [The Muse](http://themuse.ca) and Recess Weekly in St. John's, Newfoundland.

I sometimes create crossword puzzles for employees at Shopify.

## About this site

This site is built with the static site generator [Middleman](https://middlemanapp.com) and is hosted using [Netlify](https://www.netlify.com) and [Cloudflare](https://www.cloudflare.com).

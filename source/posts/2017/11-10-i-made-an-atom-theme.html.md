---
title: I made an Atom theme
description: I've tried a lot of different Atom themes and settled on making one for myself.
---

> Check out [agnostic-dark-syntax](https://atom.io/themes/agnostic-dark-syntax).

I love Atom and have been using it as my primary text editor for a long time. I bounce between syntax themes for my code every few months. I like colour in my code, but not too much, so the default theme [One Dark](https://atom.io/themes/one-dark-syntax) and some of the popular choices like [Monokai](https://atom.io/themes/monokai) are too bright for me.

For a while I used [Behave](https://atom.io/themes/behave-theme) but I again felt that there was too much going on. Then I settled on [Eclectic](https://atom.io/themes/eclectic-syntax).

I really love the Eclectic theme but had a few issues with it — some background colours seem off when you open Atom's settings, and there is a little too much colour when opening HTML files.

So I grabbed most of the colours from Eclectic, shifted some values around, and plopped them into a forked version of Atom's default theme, One Dark.

I call the result [**Agnostic Dark**](https://atom.io/themes/agnostic-dark-syntax), and I hope you'll give it a try. It supports most languages pretty well.

![](/assets/images/screenshots/code/agnostic-dark-syntax.png)

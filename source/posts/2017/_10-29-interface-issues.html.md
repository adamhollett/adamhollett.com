---
series: A better World of Warcraft interface
title:  Interface issues
part:   1
description: Looking at some of the issues with World of Warcraft's default user interface.
---

I used to play a lot of World of Warcraft. Unlike most players, I spent a lot of time on something that doesn't come with rewards of exploration, treasure, or gold. I spent time customizing my game's user interface.

One of the things WoW offers that other massively multiplayer games don't is the ability to completely customize your in-game interface. Users can create and share modifications using a [Lua](https://www.lua.org/) API.

This is a good thing, because I have a lot of issues with the game's default user interface.

![A screenshot from World of Warcraft with the default user interface](/assets/images/wow/interface/default/all.jpg 'World of Warcraft's default user interface')

In this series, I'll outline some problems with the default user interface and ways to correct them for each interface element in turn.

Note that I'll only be dealing with the _heads-up display_ — the stuff that's on the screen at all times — not with the game's toggled information panels.

## Interface elements

The most important parts of the user interface are, clockwise from the top left:

### Unit frames

![Default unit frames showing the player, a human warrior, and the target, a wolf](/assets/images/wow/interface/default/unit-frames.png)

The **unit frames** give essential health and energy info about the player and the player's current target. This is important information to know in a fight so that you know when you need to heal yourself (health) and when you can use your abilities (energy).

### Minimap

![Default minimap showing a top-down view of the area around the player](/assets/images/wow/interface/default/minimap.png)

The **minimap** shows a top-down view of the landscape around the player, as well as the current zone and the time. Buttons placed around the minimap allow access to an event calendar, tracking controls, and zoom buttons.

### Action bars

![Default action bars showing the players abilities, menu bar, and bags](/assets/images/wow/interface/default/action-bars.png)

The **action bars** list the player's chosen special abilities. These can be anything from combat moves to healing spells to single-use items. The default action bars also show a main menu with buttons for most of the game's main information panels, and buttons for each of the player's inventory bags.

### Chat

![Default chat box showing scroll buttons and two chat tabs](/assets/images/wow/interface/default/chat.png)

The chat frame shows incoming messages for the player and the current region. Separate tabs can show more specific information like guild communications and a log of all combat events.

## The problems

I have a few issues with the way things are by default.

### Location

While playing the game, you'll mostly be looking at your player or their objective, usually the current target or something in the distance like a town or a mailbox.

These essential frames are placed at the edges of the screen, as far as possible from where your eyes are on the screen. This means that to get information like your current health or which key you need to press for an ability, your eyes need to go somewhere else, and in different directions for different types of information.

If the relevant information was closer to the player, and grouped together, it would be a lot easier to see important updates without taking your eyes off the action for too long.

### Extra information

There is a lot of info here that we don't need. A player usually does not need to see their character's name on the screen. The minimap shows the current zone name, which a player will almost always know (and can easily find by opening the map).

After some time, most players will learn the hotkeys for the panes accessible from the main menu, so that element is almost unnecessary. And the chat frame is useful but doesn't need to be a large, permanently visible box, even if it is transparent.

The default action bar also has slots for twelve action buttons, and the hotkeys for those actions run across the number keys from `1` to the equals sign `=`. This is a lot of buttons, and there's no way most players can reach all the way over to the equals sign with their left hand.

### Flair

Last and probably least, there is a bit of flair that doesn't do anything except take up screen space. The gryphons on either side of the action bar, the thick minimap border, and the portraits of the player and their target really don't need to be there. I don't need to see what my player looks like — I'm already looking at it in the center of the screen.

## Solutions

The default user interface is usable but not great. With a few fairly small changes, we can move the relevant information somewhere more central and help keep the player focused on what's happening in the game, not in the interface.

Over the next few posts, I'll be looking at each of these main elements and how we can modify them to bring everything together and fix the problems above.

---
title: Move your taskbar to the side of your screen
description: We have more horizontal pixels than vertical ones. It makes sense to move your taskbar or dock to the side of your screen to maximize your vertical space.
---

The Windows taskbar has traditionally been on the bottom edge of the desktop by default. This has been the case since Windows 95. Most people know that you can move the taskbar, but not many people actually do it.

![Windows desktop with taskbar on left side](/assets/images/screenshots/windows/side-taskbar.jpg)

It's time we ditched the idea of a bottom-edge taskbar and moved it to the right or the left edge of the screen.

Vertical screen space is important. Documents on computers scroll vertically -- we look up and down to navigate long documents. Horizontal movement, in terms of both the scrollbar and our eyes, is minimal. In recent years, widescreen displays have become more popular and now ship on nearly all laptops. Most standalone monitors are also now widescreen.

Vertical screen space, in monitor resolutions, has always been less than horizontal. Twenty years ago, 640x480 and 800x600 were probably the most common desktop resolutions. Nowadays many people have  evolved to 1366x768 or 1600x900 on laptops, or perhaps 1920x1080 or higher on a desktop monitor. This means that the number of horizontal pixels (the first number) has increased nearly threefold while the vertical pixels (the second number) have doubled at best. Our screens are getting wider faster than they are getting taller.

The taskbar is about 60 pixels tall in Windows Vista, 7, and 8. In Windows XP and older versions, I'd guess that it is about 32-40 pixels tall. By keeping the taskbar on the bottom of your screen you are sacrificing roughly 5% of your vertical screen space to the taskbar.

Moving the taskbar to the left or ride side of the screen frees up that vertical space while still allowing you to access all of the buttons and information on the taskbar. I've never been a fan of hiding the taskbar outright -- it feels a little too bare and minimal for me -- so moving it to the side of the screen presents a nice balance between functionality and cleanliness.

Taking back our precious vertical pixels just makes sense. In a world where everything scrolls up and down, it's important that we maximize the amount of space we have to work with.

## How to move the Windows taskbar

1.  Right-click the taskbar. If **Lock the taskbar** is checked, click it to unlock the taskbar.

2.  Click-and-drag the taskbar toward the edge of the screen where you would like it to be. When you get close enough to the desired edge, the taskbar will pop to its new location.

3.  If you like, right-click the taskbar again and click **Lock the taskbar** to lock it in its new position.

Alternatively, right-click the taskbar, click **Properties**, and select a new position from the **Taskbar location on screen** menu.

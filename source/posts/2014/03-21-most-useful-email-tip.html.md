---
title: The most useful email tip
description: Searching for the word 'unsubscribe' is an easy way to filter out the most annoying emails.
---

I can't remember where I first read about this, but the following is probably the single easiest way to sort your emails if you have a busy inbox.

Here's the short version:

> Create an inbox filter that checks incoming emails for the word **unsubscribe**. Put any email that matches the filter into a folder called **Bulk**.

It's as easy as that.

Most mass emails include a link to unsubscribe. By searching for this word, you can identify nearly all of these emails that arrive in your inbox, and separate them from everything else. This way, see email that matters first, and other, less important messages are put in another folder.

Gmail has been moving in this direction, and now offers a different inbox that sorts your email automatically according to categories like *Social*, *Promotions*, and *Updates*, but in my case I like to be in total control of how my emails are sorted. I'd rather set up the rules myself than leave it to Gmail.

## Implementing the filter in Gmail

1.  Log on to your Gmail account.

2.  Click the **gear icon** in the upper right, then click **Settings** from the dropdown menu.

3.  Your Settings page will be displayed. Click the **Filters** tab at the top.

4.  Your email filters will be displayed. If you haven't set any up, there won't be much here. At the bottom of the Filters page, click **Create a new filter**.

5.  A panel will appear with several boxes to be filled out. In the box labeled **Includes the words**, type `unsubscribe`.

6.  Leave everything else untouched and click **Create a filter with this search** in the lower right corner of the panel.

7.  In the next panel, click to check **Skip the Inbox (Archive it)** and **Apply the label:**.

8.  Click the **Choose label...** box next to Apply the label, then choose the label you would like to apply to emails that match the filter. If you don't already have a label for bulk emails, click **New label** and type a name for your new bulk email label.

9.  If you would like this filter to apply retroactively (to emails you have already received), click to check **Also apply filter to matching conversations**.

10. Click the **Create filter** button to create your new filter. The rule will go into effect immediately. If you checked the option to apply it retroactively, your old bulk emails will be sorted according to the filter.

Enjoy your new, clutter-free inbox! It's like having a separate mailbox just for pamphlets and junk mail.

---
title: How to disable OneDrive (SkyDrive) in Windows 8.1
description: How to completely disable OneDrive from running on your Windows system.
---

Since Windows 8.1, Microsoft has included its own cloud storage solution *OneDrive* (formerly SkyDrive) as an always-running, always-available place to store your files. The problem? Some of us already use a different cloud storage service, and there's no need to have two.

Unfortunately, Microsoft doesn't allow you to uninstall the OneDrive app -- or even to right-click its system tray icon and shut it down. Microsoft doesn't ask whether we want to use their service or not.

But OneDrive can be disabled by editing the system registry.

The registry is a back-end system of data and settings that Windows uses to store information on programs, user preferences, and whatnot. The registry consists of keys (think *folders*) and values (think *files*). By editing a particular registry value, we can tell OneDrive that we don't want it to run.

## How to disable OneDrive in Windows 8.1

1.  Press **Windows Key + R** to open the Run dialog.

2.  In the dialog, type `regedit` and press the **Enter** key or click **OK**.

3.  A User Account Control prompt may ask you for confirmation. Click **Yes**.

4.  In the Registry Editor, click the arrows next to the folders (keys) in the left pane to expand them. Expand **HKEY_LOCAL_MACHINE** > **SOFTWARE** > **Policies** > **Microsoft** > **Windows**.

5.  Right-click the Windows registry key (folder) in the left pane and select **New** > **Key**.

6.  Type the name `SkyDrive` for the new key and press **Enter**.

7.  Click the new SkyDrive key that you created in the left pane to open it in the right pane. It should be mostly empty.

8.  Right-click the empty space in the right pane and select **New** > **DWORD (32-bit) Value**.

9.  Type `DisableFileSync` as the name for the new value and press the **Enter** key.

10. Double-click the new DisableFileSync value to edit it. An edit window should appear.

11. In the *Value Data* box, type `1`. Click **OK** to close the edit box and save the value.

12. Close the Registry Editor and return to Windows.

That's it! The next time you restart your computer, OneDrive should not run automatically, and OneDrive should no longer display automatically in the sidebar of Windows Explorer. You might still see it pop up every now and again in obscure places (like the Save menu in Office programs), but in general it will be much less intrusive than before, and it won't be running all the time.

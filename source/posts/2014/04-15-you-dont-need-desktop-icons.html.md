---
title: You don't need desktop icons
description: It's easy to keep a clean desktop and not need any desktop icons when you learn other ways to launch apps.
---

I secretly cringe inside when I see someone's computer and their desktop is littered with icons, folders, and shortcuts. To me, it's the equivalent of walking into someone's office and seeing three years of work piled on top of their desk, with only a haphazard attempt at organization.

It's 2014. You don't need to put anything on your desktop anymore, including desktop icons.

Here's my desktop:
![My Windows desktop, without icons](/assets/images/screenshots/windows/desktop-without-icons.jpg)

That's it. I have a few programs pinned to my start menu (Chrome, Outlook, Word, and Steam) because I use them often, but I could even do without those.

In Windows XP I used an app called [Launchy](http://www.launchy.net). Using Launchy, a key combination would bring up a window, into which you could type the first few letters of a program. Launchy would guess which program you meant and complete the name, and you could press Enter to launch the program. This way, you could launch programs very quickly without using the Start menu or desktop icons.

Here's the fun thing: since Windows 7 (maybe even Vista), this functionality has been built into Windows by default. Try it now. Think of a program you'd like to run, press the Windows Key, and type a few letters of that program's name. The Start menu should pop up and suggest the program you're looking for. No need for third-party apps anymore.

As for files and folders stored on your desktop? Stop that. You should be storing them somewhere else. The desktop is designed -- just like a real desktop -- to hold files and folders you're currently working with. It's a temporary workspace that should be kept clean and tidy. I'll store downloaded things on my desktop every now and then, but when I'm done with them, I'll put them somewhere more permanent. If your documents are important at all, consider using cloud storage or some sort of backup service.

---
title: Sync Atom between OS X and Windows
description: How to keep your Atom settings and packages synchronized across devices and operating systems.
---

I love [Atom](//atom.io). It's an excellent text editor and has replaced Sublime Text as my editor of choice.

I use Atom at work and at home. I use the same settings in both installations, so I wanted a way to synchronize them. If I find a package or theme that I like at work, I want it to be automatically available in my home installation of Atom.

Atom stores its settings in a folder called `.atom` in your user directory in both OS X and Windows. By using cloud storage and a feature of both operating systems called [symbolic links](//en.wikipedia.org/wiki/Symbolic_link) or *symlinks*, you can let your cloud storage take care of syncing your Atom settings while letting your operating system think that it exists in its usual place.

## Moving your installation into the cloud

To sync your installation across devices, the first step is to move the Atom installation that you want to keep into a cloud storage folder.

- [I want to keep my OS X installation](#os-x)
- [I want to keep my Windows installation](#windows)

### OS X

In OS X, your Atom settings will be located in a hidden folder in your home directory, typically `~/.atom`. Since you normally can't see hidden folders in OS X, the easiest way to move this folder is with a terminal command:

```bash
mv ~/.atom ~/Dropbox/.atom
```

The `mv` command moves your Atom settings folder into the folder `Dropbox` within your home directory. You can customize this command to fit your own setup if you use a different cloud storage app or you want to store the settings in a different place. I use `~/Dropbox/Settings/.atom`.

### Windows

In Windows, your Atom settings are stored in a folder called `.atom` in your user directory. Since you can see this folder by default in Windows, you can easily copy and paste it into your cloud storage folder. In Windows, I put it in `C:\Users\Adam\Dropbox\Settings\.atom`.

### Delete the unsynced settings folder

Since you'll be syncing your Atom settings from the version in your cloud storage, you can temporarily delete the version that exists in Atom's default settings location.

- in OS X, delete the folder `~/.atom`
- in Windows, delete the folder `C:\Users\YourName\.atom`

### Selective syncing

You don't need to sync every file in the `.atom` folder. Atom's developers considered the fact that people might want to sync their settings folders or commit them to source control, so they provided a `.gitignore` file that specifies which files can be excluded:

```text
storage
compile-cache
dev
.npm
.node-gyp
```

This `.gitignore` file explains that the files and folders in the list can be skipped when syncing or committing the `.atom` folder. You can use this list to tell your cloud storage app not to sync those files and folders.

Here's how to ignore those files in Dropbox:

1.  Right-click the Dropbox icon in your menu bar (OS X) or system tray (Windows).

2.  Click the **gear icon**, then click **Preferences**.

3.  Click the **Account** section, then click **Selective Sync**.

4.  The Selective Sync window lists all the files in your Dropbox. Expand the `.atom` folder that you moved into your Dropbox earlier, and uncheck the following items:
    - `.node-gyp`
    - `compile-cache`
    - `storage`

5.  Click **Update** to confirm your changes.

Now your Dropbox will only sync the files that are essential to your local Atom installation, and won't waste time syncing the unimportant ones.

## Creating the symlink in OS X

Creating a symlink in OS X makes your operating system think that the Atom settings files in your Dropbox actually exist in their proper place in your user folder.

The best way to create a symlink is, again, from the Terminal:

1.  Press `command` + `space` to open Spotlight.

2.  Type `terminal` and press `enter` to launch Terminal.

3.  When the Terminal prompt appears, type the following command and press `enter`:

    ```bash
    ln -s ~/Dropbox/.atom ~/.atom
    ```

    The `ln` command creates a symlink, in this case, from `~/Dropbox.atom` to `~/.atom`. You may have to edit the first path to reflect where in your Dropbox you placed your `.atom` folder.

This makes the files in your Dropbox *appear* to be where they should be in your user folder, so your Atom installation on OS X can read and use the files even while they are being synced in Dropbox.

## Creating the symlink in Windows

Creating a symlink in Windows works in much the same way, though the command is different.

You need to use the Windows Command Prompt to create a symlink in Windows:

1.  Press the `windows key` and type `command` to search for the Command Prompt.

2.  In the search results list, right-click the **Command Prompt** entry and click **Run as Administrator**.

3.  When the Command Prompt appears, type the following command and press `enter`:

    ```batch
    mklink /J "C:\Users\Adam\.atom" "C:\Users\Adam\Dropbox\Settings\.atom"
    ```

    You'll need to edit the above command to correspond to your user folder and Dropbox folder. The first path points to where your Dropbox Atom files will be symlinked *to*, and the second path is where they will be symlinked *from* (that is, your cloud storage).

Like in OS X, this command makes the files in your Dropbox *appear* to be in the location where Atom reads them.

## Conclusion

I've only had this running for a few days as of writing this, but things are working as they should so far. If I install an Atom package at work, it's available on my Windows installation when I get home. Your settings, packages, and themes should stay in sync fairly consistently using symlinks.

---
title: Create web app shortcuts with Chrome
description: Learn how to turn any web app into a native app using Google Chrome.
---

Google Chrome has a neat feature that lets you create web app shortcuts to run web apps in their own window, without the usual toolbars and interface of a browser. This means that you can pick and choose your favourite web apps and "install" them as native apps on your computer. These native apps will launch in their own window and function like any other program.

Web apps these days are pretty impressive, from [text and Markdown editors](https://stackedit.io/editor) to [music players](http://rdio.com) and even [development environments](https://codeanywhere.com). With Chrome, you can create application links to your favourite web apps.

## How to create a web app shortcut in Chrome

1.  In Chrome, navigate to the web app that you'd like to create an application shortcut for.

    For example, go to `simplenote.com` for the note-taking app Simplenote.

2.  If required, log in to the web app.

    Logging into Simplenote brings you to `app.simplenote.com`, which is where we want our application shortcut to lead. You want your application shortcut to point to the app itself, not a login or introduction page.

3.  In Chrome, click the **Menu** button, then under **More tools**, click **Create application shortcuts**.

    Chrome will ask you where you want to create shortcuts. These options will differ depending on your operating system. **Desktop** is usually a pretty safe choice).

4.  Select where you would like shortcuts to be created and click **OK**. Chrome will create one or more shortcuts to the web app on your computer.

![Simplenote as a native app](/assets/images/screenshots/windows/simplenote-chrome-app.png)

When you launch a web app shortcut, the app will open in its own window, without the Chrome browser interface. You can move the web app shortcut anywhere you like on your computer and it will still work. I would suggest [moving it off your desktop](/posts/2014/04/you-dont-need-desktop-icons "You don't need desktop icons").

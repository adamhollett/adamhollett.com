---
title: My gym routine
description: The weight routine I use at the gym to feel better in just a few hours per week.
---

Last fall I decided to try to get into better shape, so I started going to the gym and lifting weights regularly. I've had some great results and wanted to share the routine that finally worked for me.

I learned most of these lifts a few years ago from a friend. Most of them are pretty straightforward but if it's your first time I would get some initial help for the bigger lifts like squats and deadlifts.

This routine takes me around an hour three times a week. It's relatively quick.

## General advice

You should aim to lift **three times a week**. Always take at least one day to rest between workouts — you'll need it. Don't do this workout two days in a row or you'll hurt yourself.

The _single most valuable thing_ you can do when starting out is to set a schedule and stick to it strictly until it becomes a habit. If you don't go three times a week, or you frequently skip the gym to do other things, you will not form good habits and you will not see results.

This routine uses free weights, not machines. Some of the lifts have dumbbell (two weights) and barbell (one weight) versions. When you're able, do dumbbell exercises for a better workout.

You don't _need_ to stretch before or after this workout, but some light stretches after you're finished can help offset muscle soreness on your rest days. I walk a lot, so I don't include any cardio, but you can warm up with a few minutes of cardio before you start if you like.

## The routine

Most of the following is based on [Sean10mm's Starting Strength routine](https://newbie-fitness.blogspot.com/2006/12/rippetoes-starting-strength.html), modified to be a little less intense.

The routine consists of two alternating workouts of four exercises each, with a couple of supplementary lifts added at the end of the week.

Where two exercises are listed, the first one is more difficult (and more effective).

### Workout A

- 3x5 [barbell deadlift](https://exrx.net/WeightExercises/GluteusMaximus/BBDeadlift) [^deadlifts]
- 5x5 [dumbbell bent-over row](https://exrx.net/WeightExercises/BackGeneral/BBentOverRow) or [barbell bent-over row](https://exrx.net/WeightExercises/BackGeneral/BBBentOverRow)
- 5x5 [dumbbell shoulder press](https://exrx.net/WeightExercises/DeltoidAnterior/DBShoulderPress) or [barbell military press](https://exrx.net/WeightExercises/DeltoidAnterior/BBMilitaryPress)
- 3x5–8 [bodyweight triceps dip](https://exrx.net/WeightExercises/Triceps/BWTriDip) or [assisted triceps dip](https://exrx.net/WeightExercises/Triceps/ASTriDip)

[^deadlifts]: For deadlifts, do 3x5 but the first two sets should be warm-up sets. This means you should **only do one set of deadlifts at your max weight**.

### Workout B

- 5x5 [barbell squat](https://exrx.net/WeightExercises/Quadriceps/BBSquat)
- 5x5 [dumbbell bench press](https://exrx.net/WeightExercises/PectoralSternal/DBBenchPress) or [barbell bench press](https://exrx.net/WeightExercises/PectoralSternal/BBBenchPress)
- 3x5–8 [bodyweight pull-up](https://exrx.net/WeightExercises/LatissimusDorsi/BWPullup) or [assisted pull-up](https://www.exrx.net/WeightExercises/LatissimusDorsi/AsPullupKneeling)
- 3x10 [weighted incline crunch](https://exrx.net/WeightExercises/RectusAbdominis/WtInclineCrunch) or [bodyweight incline crunch](https://exrx.net/WeightExercises/RectusAbdominis/BWInclineCrunch)

### Supplementary

Supplementary exercises are added at the end of your last workout for the week.

- 5x5 [dumbbell lying triceps extension](https://exrx.net/WeightExercises/Triceps/DBLyingTriExt)
- 5x5 [dumbbell curl](https://exrx.net/WeightExercises/Biceps/DBCurl) or [barbell curl](https://exrx.net/WeightExercises/Biceps/BBCurl)

### Example schedule

If you decide that your gym days are Monday, Wednesday, and Saturday:

Week 1:

- Monday: Workout A
- Wednesday: Workout B
- Saturday: Workout A + supplementary workout

Week 2:

- Monday: Workout B
- Wednesday: Workout A
- Saturday: Workout B + supplementary workout

### What the numbers mean

The first number before each exercise is the amount of "sets" you will do. A set is a group of lifts with no significant rest in between. Between each set, you should rest for a minute.

The second number is the number of repetitions, or "reps". This is how many lifts you perform in a set before resting.

So "3x5" means you would do 5 reps of that exercise, rest for a minute, do 5 more reps, rest for a minute, and finish off with a final 5 reps.

For the **5x5** exercises, you should do two warm-up sets at lower weights, and then three sets at your max weight. For example, if your max weight for a bench press is 60 lbs, you might do:

- 5 x 20 lbs
- 5 x 40 lbs
- 5 x 60 lbs
- 5 x 60 lbs
- 5 x 60 lbs

Don't skip warm-up sets or you might hurt yourself. Warm-up sets are especially important for squats and deadlifts.

### How much to lift

When you start, take some time to figure out the maximum amount you're able to do for each lift. Keep your weights low when you're starting out so as to not overexert or injure yourself.

Bring a notebook or a workout tracking app to the gym and record how much you lift for each set of each exercise. I use [Fitnotes](https://play.google.com/store/apps/details?id=com.github.jamesgay.fitnotes) on Android.

Alongside your lift numbers, write down whether the exercise as a whole was easy or hard. Did you struggle to complete your last set or two, or was it a breeze?

At the beginning of your week, look at your lifts from the week before. If they were easy, increase your weights for the coming week. For dumbbell exercises, add 5 pounds. For barbell exercises, add 10 pounds. If a lift was too tough, don't increase its weight for the coming week.

It may take a few weeks to settle into your natural max weights for each lift, but that's okay. Doing it this way will eventually bring you to a naturally challenging weight for each lift. After a while, you'll only increase your weight every few weeks, but you should always be challenging yourself without overexerting.

Watching these numbers go up as you get stronger, for me, is _the most exciting part of lifting_. When you start out, you'll find it difficult to stand with an empty barbell (40 lbs) on your shoulders. After a few weeks, that barbell will feel like it weighs nothing and you'll be easily squatting a hundred pounds.

Above all, make sure you're challenging yourself every time you visit the gym, but also _listen to your body_. If something feels weird, or hurts, or just doesn't seem right, stop, move on to something else, and try that exercise again next visit.

---

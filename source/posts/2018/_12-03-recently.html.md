---
title: Recently
---

I'm always busy, but I've been extra busy these last few months.

## Podcasts

I listen to podcasts while walking to and from work and while at the gym. This gives me a few hours a week to get through my favourites.

-   [**Heavyweight**](https://www.gimletmedia.com/heavyweight) — Host Jonathan Goldstein tracks down people with something unresolved in their past, and helps them resolve it. Super quirky and charming.
-   [**Terrible, Thanks for Asking**](https://www.apmpodcasts.org/ttfa) — Nora McInerny interviews people who have been through some seriously awful stuff. This one sounds like a downer but it's often uplifting, hilarious, and eye-opening.
-   [**The Heart**](https://www.theheartradio.org) — Host Kaitlin Prest creates blissfully real audio stories. [The miniseason titled "No"](https://www.theheartradio.org/no-episodes) looks back at her relationship with consent throughout her life and is one of the most intense things I've ever listened to.

## Music

I've been on another mashups kick recently, re-listening to a lot of my old favourite albums.

- [**The Art of Noise**](https://soundcloud.com/bruneaux/sets/bruneaux-the-art-of-noise) by Bruneaux
- [**All Day**](https://soundcloud.com/mstrflsh/girltalk_allday) by Girl Talk

## Reading

I don't read as often as I'd like to, but I currently have bookmarks in:

- **The Anthologist** by Nicholson Baker
- **The Subtle Art of Not Giving a F*ck** by Mark Manson
- **Kitchen Confidential** by Anthony Bourdain

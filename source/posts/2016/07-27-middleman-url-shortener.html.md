---
title: Making a basic URL shortener with Middleman
description: How I used a shortened version of my domain and dynamic pages to make my own URL shortener.
---

About a year ago, I bought [admh.lt](http://admh.lt), a shortened version of my adamhollett.com domain name that also happens to be the slug I use for my username on [many](https://github.com/admhlt) [sites](https://twitter.com/admhlt). admh.lt just redirects to adamhollett.com, but it retains anything else that's been appended to the URL, so `admh.lt/posts` redirects to `adamhollett.com/posts`.

I've always thought that it would be fun to roll my own URL shortening service with this tiny domain name. Since my site is built using Middleman now, I decided to see if there was a way I could use dynamic pages to generate basic HTML pages that would redirect to external URLs.

It turned out to be fairly simple to generate [redirecting HTML pages](http://stackoverflow.com/questions/5411538/redirect-from-an-html-page) [dynamically with Middleman](https://middlemanapp.com/advanced/dynamic_pages/). And the whole process only takes about 20 lines of code -- and that number includes the HTML template used for each redirect page.

## Creating the list of shortlinks

The easiest way to store the links will be in a simple YAML list, so create one in your site's `data` folder. As an example, I'll create shortlinks to some of my favourite static site generators.

Create a file in your project called `/data/shortlinks.yml`. This will let you access the links object with Middleman using `data.shortlinks`.

The YAML file has a simple `key: value` format. The `key`s will be the slugs you append to your short URL, and the `value`s will be the web addresses they point to:

~~~ yaml
middleman: https://middlemanapp.com/
jekyll: http://jekyllrb.com/
hugo: http://gohugo.io/
~~~

In the future, you'll be able to update your shortlinks just by editing this file and rebuilding your site.

## Creating the function in config.rb

Next, we'll create a method in `config.rb` that generates a redirect page using each of the entries in `shortlinks.yml`:

~~~ ruby
# Generate shortlinks
data.shortlinks.each do |link|
  proxy "/#{link[0]}/index.html", "/shortlink.html", layout: false, locals: { destination: link[1] }, ignore: true
end
~~~

I'll try to explain what's going on in each part of this loop:

`"/#{link[0]}/index.html"` tells the method to create an index.html file in directories using the names of our keys in `shortlinks.yml`. We'll get:

- `middleman/index.html`
- `jekyll/index.html`
- `hugo/index.html`

`"/shortlink.html"` tells Middleman which file to use as the template for each of the dynamically-generated pages. Oddly, it doesn't seem like you can use one of your normal Middleman `layout`s for this file. We'll create `shortlink.html.erb` in [the next step](#creating-the-template). Note that within the method you have to refer to the template file without including any of its templating extensions, so if your file is called `shortlink.html.erb`, here you'll just write `shortlink.html`.

`layout: false` tells Middleman not to bother using a layout file to render these dynamic files.

`locals: { destination: link[1] }` tells Middleman to pass the values of our items in `shortlinks.yml` as parameters to the template we just specified. I've given the values the name `:destination`, which is the name we'll use to reference them in the template file.

`ignore: true` tells Middleman not to render our template file `shortlinks.html.erb` into its own page in the sitemap.

## Creating the template

The last step is to create the template file for the method to use to generate the dynamic redirect pages. I've called mine `shortlink.html.erb`, but you can call it whatever you like, and place it where you like. In my case, I just put it with the rest of my top-level pages.

You can make a redirecting HTML page with just a few lines of code:

~~~ erb
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="1;url=<%= destination %>">
    <script type="text/javascript">
      window.location.href = "<%= destination %>"
    </script>
    <title>Redirecting...</title>
  </head>
  <body>
    <p>Redirecting to <%= destination %>...</p>
  </body>
</html>
~~~

This small file is just enough code to redirect users with JavaScript, and with an `http-equiv="refresh"` tag if JavaScript isn't available. The URLs for our shortlinks are passed in to the template and referenced with `<%= destination %>`.

## Conclusion

Now that you've written all the code, you should be able to build your Middleman site and generate as many shortlinks as you like. You can test the redirects while running Middleman as a server with `middleman server` before you build your site.

I'm giving a short talk this week at work on how to tie your shoelaces quickly using the Ian Knot, and I wanted to be able to direct the audience to check out more information using the short URL [admh.lt/ianknot](http://admh.lt/ianknot). So I built this. Check it out!

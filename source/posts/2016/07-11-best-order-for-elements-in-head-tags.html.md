---
title: Best order for elements in HTML head tags
description: Learn an optimal way to order elements in your page head.
---

I can't take credit for this, but it's something I refer to often in my bookmarks. In [this StackOverflow discussion](http://stackoverflow.com/questions/1987065/what-are-best-practices-to-order-elements-in-head), someone asked what the best order was for elements in the `<head>` tag of an HTML page.

The order of most elements in the `<head>` doesn't _really_ matter. It's a matter of preference, and they should be ordered any way you like.

Matej Janovčík provided [this sample `<head>` tag](http://stackoverflow.com/a/30766041) with a tidy order as an example. I've made some minor adjustments and I use this snippet a lot, so I continue to update it as things change. You can add or remove whatever you like, or expand on this if you need more metadata:

~~~ html
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title></title>

  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="keywords" content="">
  <meta name="robots" content="index, follow">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <link rel="stylesheet" href="">
  <link rel="icon" href="">
  <link rel="canonical" href="">

  <meta property="og:site_name" content="">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:locale" content="">
  <meta property="og:image" content="">

  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:creator" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:image" content="">

  <script src=""></script>
</head>
~~~

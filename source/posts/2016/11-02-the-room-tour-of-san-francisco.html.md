---
title: The Room tour of San Francisco
description: I went to San Francisco and visited a bunch of places that appear in the 2003 movie 'The Room'.
---

<figure>
  <iframe src="https://www.google.com/maps/d/u/1/embed?mid=1I3DaXEKbhDvqufiHFLZzWxO3id8" width="100%" height="480"></iframe>
</figure>

[See the map in full screen](https://www.google.com/maps/d/u/0/viewer?mid=1I3DaXEKbhDvqufiHFLZzWxO3id8)

I recently traveled to San Francisco on business, attending Accessibility Camp Bay Area and meeting with a few people I've worked with remotely over the last few months. It was a great trip and it was my first time in the city.

I also happen to be completely obsessed with [The Room](http://www.imdb.com/title/tt0368226/), the quintessential "so bad it's good" movie of the last few decades. The Room was directed by Tommy Wiseau, an enigmatic and strange man who for some years called San Francisco home. Although it was filmed in Los Angeles, the movie is set in San Francisco, and proves that with many repetitive establishing shots and outdoor scenes.

I found [this map](https://www.google.com/maps/d/u/0/viewer?mid=18bO9Ch7C5Sy7ZyeN88_HuClulqc) that someone made which details some of the shots filmed in San Francisco, and decided to visit and photograph whichever ones I could. In the process, I discovered that the map isn't perfect. A few locations are missing or incorrect.

So I decided to make my own, both to keep track of the locations I'd visited and photographed, and to try to improve on the original idea by adding information alongside each marker, like trivia about the film relevant to the location.

## Day 1

I stayed downtown, in the financial district, just north of Union Square. I had one day set aside for exploring, so on that day I walked east to the Embarcadero, then up along the waterfront past Fisherman's Wharf and eventually to the Palace of Fine Arts near the Presidio. I visited [The Exploratorium](https://www.exploratorium.edu) on the way.

I stopped by the location from the famous coffee shop scene, and learned from the clerk there that Tommy Wiseau still owns and manages the building that houses the coffee shop, which is now a pizza joint called Pizza Zone. The friendly man working there (I was the only customer) spent a few minutes showing me DVD copies of The Room that he had behind the counter, and a shelf of Tommy Wiseau underwear. The pizza wasn't bad either.

![A slice of pizza on a paper plate](/assets/images/the-room-locations/coffee-shop-pizza.jpg)

Outside that building, there's a huge poster advertising The Room with the infamous photo of Tommy Wiseau's face, and a giant sculpture of a pair of jeans, an homage to Tommy's clothing company Street Fashions USA.

![A building with a large poster for The Room and a scuplture of blue jeans](/assets/images/the-room-locations/coffee-shop-outside.jpg)

I continued west, past Fort Mason into the Marina district, which houses the Palace of Fine Arts and the house meant to be Johnny and Lisa's apartment in the movie. It's actually just someone's home, and Wiseau filmed several shots outside the house without asking the permission of the homeowners or getting a permit. In the interests of not intruding too much on someone's home, I snapped a quick selfie from across the street with the house in the background.

![The author with a brick house on Broderick Street in the background](/assets/images/the-room-locations/house-on-broderick.jpg)

At this point, I almost turned back because I wasn't sure how far it was to any other locations. But I decided to walk one more block west, and was surprised when the Palace of Fine Arts turned out to be right next to the house. I got some photos and spent some time walking around the palace and the rotunda.

![San Francisco's Palace of Fine Arts](/assets/images/the-room-locations/palace-of-fine-arts.jpg)

The palace is next to Lyon Street, which features some steep steps that Johnny and Mark jog up during a morning run. I decided to head there as my last stop for the day, and began an arduous climb up Lyon Street to the base of the stairs. As if the hilly street wasn't enough of a climb, the stairs proved to be very steep and there were quite a few of them. At the top, a man with a dog was nice enough to snap my picture.

![The author standing at the top of a steep set of stairs](/assets/images/the-room-locations/lyon-st-steps.jpg)

I had walked about seven miles that day, so I called an Uber and headed back.

## Day 2

I had to work for the rest of my time in the city, but I still had a few things to see, so for the next few days I woke up early and went for walks before work. On my second day of exploring, I took the subway down to the Mission, which is the location of the building used in the flower shop scene in the movie.

I walked west on 16th Street for quite a long time, but I couldn't figure out which building was the right one, and it didn't seem to be where the map said it was. I snapped a picture of the closest candidate, and popped into a restaurant called [Kitchen Story](http://kitchenstorysf.com/) (try their "Millionaire's Bacon") for breakfast before heading to work.

![A building on 16th Street in San Francisco](/assets/images/the-room-locations/flower-shop-wrong.jpg)

I did some research that night and used Street View to figure out where I'd gone wrong. It turns out I had looking on the wrong side of the road, and had in fact walked right past the flower shop building twice.

Along the way, I'd walked past Guerrero Street. In the movie, Mark mentions a "hospital on Guerrero Street", but that line was ad-libbed. There's no hospital on Guerrero Street.

![A street sign at the intersection of 16th Street and Guerrero Street](/assets/images/the-room-locations/guerrero-street.jpg)

## Day 3

I returned to the Mission the next day before work, and walked on the other side of 16th Street this time. It was a bit of a trek, but I finally found the building used in the flower shop scene and took a photo. In the movie, it's a place called Anniversary Flowers and Gifts, but that's since closed. It's since been home to a couple of other businesses, but it's not currently in use and the windows are boarded up.

![A building on 16th Street in San Francisco](/assets/images/the-room-locations/flower-shop-right.jpg)

Later that day, I realized I only had one significant location left to visit: Golden Gate Park, where Johnny and Mark have a spontaneous game of football during a morning jog. It was the furthest location from where I was staying, but I knew I couldn't visit _all but one_ location. I had a meeting to attend that afternoon, so my time was limited. I called an Uber to get me across town to the park, got out near the right place, and took some photos of the field and the place where Johnny parks his car.

![A car parked on the side of the road in Golden Gate Park](/assets/images/the-room-locations/golden-gate-parking-spot.jpg)

![The author taking a picture of himself in Golden Gate Park](/assets/images/the-room-locations/golden-gate-park-field.jpg)

I only had a few minutes to spend in the park before I had to get back for my meeting, but I got the impression that Golden Gate Park is really big and really beautiful. I'd love to spend a day exploring the park the next time I'm in the city.

I was exhausted that night after doing so much walking for the past few days, and I reviewed the film to see how accurate my photos of the locations were. While reviewing it, I noticed a few establishing shot locations that the map hadn't tracked — which meant that there were places I'd missed.

To be fair, I'd managed to visit all the locations where characters from the film are shown in San Francisco. The only places I hadn't gone were shown in establishing shots. So I felt like I had finished what I set out to do. But then I realized that one of the new locations I'd found was only six blocks from my hotel.

There were two new locations I was trying to track down. In one shot, Johnny rides in a streetcar past a cathedral. I paused the movie in a couple of places to see the street signs in the shot. They were **Taylor** street and **ushman** street (the first letter was obscured by a tree). Using that info and the cathedral in the background, I figured out that the location was Grace Cathedral, on California Street, near the intersections of Taylor and Cushman.

The other location I was less sure about. It's a quick establishing shot that's used a few times, looking down a hill in a neighbourhood with a lot of skyscrapers. There were no real landmarks to figure out which street it was aside from a sign on a building that said **Academy of Arts College**. I searched that name online and didn't get any useful results. After a quick rest, I set out again one last time, knowing I could at least snap a photo of the cathedral.

The cathedral was two blocks north and four blocks west of my hotel. What I didn't realize was after the two blocks north, the other four were up a steep hill. Typical San Francisco. I made the tough climb to the top, leaving me out of breath and extra tired.

![Grace Cathedral at night](/assets/images/the-room-locations/grace-cathedral.jpg)

I turned left at the cathedral to walk down Taylor Street, taking a different route home. Just over a block down the hill, I looked up to see a sign that read **Academy of Arts University**. I did a double-take, and realized that this was the mystery establishing shot street I'd been puzzled about just a few minutes earlier. Less exciting was the fact that I had to trek back up the hill to the cathedral to get the right photo. I had come too far not to, so I walked back up a steep block and a half to snap my last photo.

![Looking south down Taylor street](/assets/images/the-room-locations/taylor-street.jpg)

That was my last night in San Francisco. All in all, I'd managed to visit all the important locations from the movie, and it had turned out to be a pretty great way to frame my exploration of the city. I'd visited the Embarcadero, Fisherman's Wharf, the Marina, the Palace, the Mission, and Golden Gate Park.

If you're a weirdo like me, and like The Room, and are thinking of visiting San Francisco, feel free to use the map I've made to go visit these places.

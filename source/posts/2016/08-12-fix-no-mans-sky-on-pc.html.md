---
title: How to (partly) fix No Man's Sky on PC
description: In some cases, you can improve No Man's Sky by disabling VSync and changing field of view and anti-aliasing options.
---

[No Man's Sky](http://www.no-mans-sky.com/) was released on PC today, and was released for the PS4 a few days ago. Negative reviews came pouring in for the PC version, which seems to be having major performance issues for a lot of players.

![Mixed reviews for No Man's Sky](/assets/images/screenshots/windows/no-mans-sky-steam-store.png)

The game seems to run fine for me, aside for some framerate stuttering on the first launch. One thing I couldn't stomach, however, was the incredibly narrow field of view. A lot of people get motion sickness from playing games with narrow fields of view.

Here are the steps I took to make **No Man's Sky** a little more palatable by editing the graphics settings file.

## Field of view

By default, the field of view is set to **75**, which might be okay if you're sitting far from the screen and playing on a TV. Since my monitor's right in front of me, I need a much higher value for things to look correct.

The in-game options let you set the field of view to values up to 100, but I found that setting even a little too narrow.

Note that **if you set the field of view value higher than 100, visiting the graphics options in-game resets the value to 100**.

To set your field of view higher than 100:

1.  Open your game's settings folder at `<path-to-game>\Binaries\SETTINGS`.

2.  Open `TKGRAPHICSSETTINGS.MXML` with a text editor.

3.  Find the `FoVOnFoot` and `FoVInShip` properties, and set them to the values you want:

    ~~~ xml
    <Property name="FoVOnFoot" value="120.000000" />
    <Property name="FoVInShip" value="120.000000" />
    ~~~

Some sources seem to recommend keeping the `FoVInShip` value at its default setting. I've found a field of view value of **140** to be my preference.

## Max framerate

By default, the maximum framerate is set to **30**, which is far too low for a modern PC game. Set this to **60**, or even higher if you have a monitor that can refresh faster than 60 Hz:

~~~ xml
<Property name="MaxframeRate" value="120" />
~~~

A setting of **Max FPS** in game sets this configuration value to **160**.

## VSync

If your framerate stutters, try setting VSync to **false** in the in-game graphics options:

~~~ xml
<Property name="VSync" value="false" />
~~~

## Anti-aliasing

Some people have said that setting their anti-aliasing to use **FXAA** improved performance:

~~~ xml
<Property name="AntiAliasing" value="FXAA" />
~~~

## Borderless fullscreen

In some cases, running the game in a borderless fullscreen window can give you better performance:

~~~ xml
<Property name="FullScreen" value="false" />
<Property name="Borderless" value="true" />
<Property name="UseScreenResolution" value="true" />
~~~

As with all things, your mileage could vary here. But editing my graphics config file went a long way toward making this game more playable.

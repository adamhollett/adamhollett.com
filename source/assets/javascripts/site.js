//= require vendor/jquery-2.2.4.min

// create <figure> with caption from <p><img>

$('article p img').unwrap().wrap('<figure></figure>').after(function() {
  if (this.title) {
    return '<figcaption>' + this.title + '</figcaption>';
  }
});

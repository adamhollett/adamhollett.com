// create table of contents from headings

if ($(".content h2").length >= 2) {

  var tableOfContents =
    "<nav role='navigation' class='table-of-contents'>" +
      "<p class='heading-3'>Contents</p>" +
      "<ul>";

  var el, title, link;

  $(".content h2").each(function() {
    el = $(this);
    title = el.text();
    link = "#" + el.attr("id");

    newLine =
      "<li>" +
        "<a href='" + link + "'>" +
          title +
        "</a>" +
      "</li>";

    tableOfContents += newLine;
  });

  tableOfContents +=
     "</ul>" +
    "</nav>";

$(tableOfContents).insertBefore(".content h2:first-of-type");

};

---
title: Information architecture
week: 5
class_date: February 8, 2017
theme: blood
---

Add me on LinkedIn!

[linkedin.com/in/adamhollett](https://www.linkedin.com/in/adamhollett)

----

Before we get started:

What's the deal with _notifications_?

----

Let's talk about

### Information architecture

----

> Good information architecture enables people to find and do what they came for. Great information architecture takes "find" out of the equation: The site behaves as the user expects.

-- [Jeffrey Zeldman](https://twitter.com/zeldman), founder of [A List Apart](http://alistapart.com)

----

What do you think of when you hear the phrase **information architecture**?

<textarea class="whiteboard" name="information-architecture" cols="40" rows="5"></textarea>

----

**Information architecture** (IA) defines how an experience is structured -- both the visible parts of the experience and the back-end (the underlying code that the experience is built on).

----

As a result, it has a great deal of influence on how users navigate though your experience.

----

Your user experience is only as strong as your information architecture.

----

But, like, what is it though?

----

IA is basically the **internal structure** and **layout** of an experience.

----

You can see IA in URLs, <span class="fragment">in the files and folders that make up an experience,</span> <span class="fragment">in the layout of components that make up an experience (like a web page),</span> <span class="fragment">and in the way users navigate and search an experience.</span>

----

But, like, <span class="fragment"><strong>what is it though</strong>?</span>

----

IA is notoriously hard to define and hard to see, but it's very important.

- [Definition from usability.gov](http://www.usability.gov/what-and-why/information-architecture.html)
- [Definition from The Guardian](http://www.theguardian.com/help/insideguardian/2010/feb/02/what-is-information-architecture)

----

[Where am I?](http://alistapart.com/article/whereami)

a post on information architecture in navigation from Derek Powazek

----

We've learned that **UX is iterative**. This means that, as UX designers, we're always adding new features or redesigning existing components to meet users' needs better.

----

But that's a lot of work.

----

### Breaking down an experience

You can make a **wireframe** of an experience to break it down into components. Good IA identifies and re-uses components in a logical way.

A wireframe is a rough approximation of the layout and contents of an experience using basic shapes and text labels.

----

For an experience that already exists, a wireframe is a good way to take inventory of content and its placement.

When building an experience, wireframes let you easily experiment with different layouts and content arrangements.

----

Prototyping or wireframing are good ways to explore new designs or design changes without investing too much time or money in actually changing things.

----

[Example wireframe of youtube.com](https://wireframepro.mockflow.com/view/youtube_com)

----

How might you wireframe [yahoo.com](https://yahoo.com)?

----

## Benefits of good IA

----

### Usability

Good IA lets users find what they need quickly and easily. It ensures that:

<ul>
  <li class="fragment">the most important content is given the highest priority</li>
  <li class="fragment">navigating through the experience makes sense</li>
  <li class="fragment">the experience behaves the way a user expects it to</li>
</ul>

----

### Design efficiency

Good IA helps separate form from function. This lets you:

<ul>
  <li class="fragment">focus on the design areas that matter most to your users</li>
  <li class="fragment">break down an experience into reusable components</li>
</ul>

----

### Maintenance costs

Since good IA breaks an experience into components, you can more easily update and change components without having to change the whole experience.

----

### Satisfaction

Users like experiences more when they are easy to use and navigate.

----

### Information architecture exercise

Examine the IA for a website:

- cbc.ca
- algonquincollege.com
- kinaxis.com
- spotify.com
- solace.com
- youtube.com
- squarespace.com
- apple.com

----

### Usability principles

Read Steven Bradley's blog entries on anthropomorphic forms ([part 1](http://vanseodesign.com/web-design/anthropomorphic-forms-part-i), [part 2](http://vanseodesign.com/web-design/anthropomorphic-forms-part-ii)) for great explanations of the **anthropomorphism**, **contour bias**, and **the uncanny valley**.

----

Next week:

<img src="https://pbs.twimg.com/profile_images/825132085455253504/TaLi-zvh.jpg" style="width: 200px; height: 200px; border-radius: 50%;">

Elizabeth "Biz" Sanford on
**content strategy**

---
title: Channels
week: 7
class_date: March 1, 2017
---

CloudFlare is a content delivery network, a distributed network of servers that helps sites on the Internet serve data to a global audience.

----

On February 17 Tavis Ormandy, a [white hat hacker](https://en.wikipedia.org/wiki/White_hat_(computer_security)), discovered that a bug in CloudFlare was essentially causing private data to leak onto sites across the Internet.

----

The incident has been called "[Cloudbleed](https://en.wikipedia.org/wiki/Cloudbleed)" because of its significance to the [Heartbleed](https://en.wikipedia.org/wiki/Heartbleed) security bug reported in 2014.

----

The leaked information included

> private messages from major dating sites, full messages from a well-known chat service, online password manager data, frames from adult video sites, hotel bookings...

----

The leak probably didn't affect you, but you might have received a notice from some services that you use advising you to update your password.

I was advised to change my Uber password.

----

Do you use Uber?

Try changing your password.

----

# Channels

----

Experiences that you work on will probably show up in many different **channels**.

----

There are a lot of channels -- more now than ever before.

- desktop computers
- smart TVs
- car displays / information systems

What else?

----

## Changing trends

![The fold](/assets/images/ux/the-fold.jpg)

----

Users might even jump between several channels multiple times to complete a task.

Notes:

Mention signup process for a streaming service like CraveTV using an Apple TV.

----

## Content considerations for multichannel

- structured -- easily identified, labeled, created, and repeated
- modular -- components that behave as independent units
- reusable -- easily reused regardless of the output channel or format
- metadata -- *data about data*
- format free -- the presentation layer controls how the content appears

----

## The multichannel approach

-   Understand user journeys and which channels require specific content for user needs.

----

-   Build your experience using reusable **modules** or **components**.

    Some modules might need to appear on certain channels and not others.

----

-  Use an adaptive approach.

    Structure your experience so that it can adapt to multiple channels. For example, a web app might use **responsive design**.

----

-   Determine global and channel-specific content.

    Some content might work on all channels. Some content might be shared between channels but require edits to work. Some content might be specific to a single channel.

Notes:

Demonstrate responsive menu and table of content for Shopify Help Center.

----

-   Conduct user tests to see how users engage with each channel.

    Don't just look at a single experience -- pay attention to how a user jumps from one channel to another. When and why do they do that?

----

-   Incrementally measure the performance of an experience so that you can continuously optimize.

    Every so often, measure how different channels are being used. How can you increase engagement with underused channels?

----

-   Test your experience with multiple devices

    Test your work using as many different devices as you can. If it's a mobile channel, test it on iOS, Android, Blackberry, or even Windows Phone. If you're making a web experience, test it in the most popular browsers.

    Many browsers [use the same rendering engines](https://en.wikipedia.org/wiki/Web_browser_engine), so it's okay to just test one browser for each engine.

----

-   Use standard coding languages

    Use popular, standard code such as HTML5, CSS, and XML. Fancy tools like preprocessors and add-ons can make things easier but will cause problems if they ever go out of style or are no longer maintained.

----

## Designing for large screens

Some content works best on larger devices:

- complex images / infographics
- guides or instructions
- long-form content

What else?

----

## Mobile-first

In response to the prevalence of smaller devices, many web designers now tend to work **mobile-first** -- that is, design primarily for mobile devices and build on that style for larger screens.

----

## Designing for mobile

Mobile devices require certain considerations:

- finger sizes vary, so buttons have to be big enough to tap
- fonts should be large enough to read on a small screen
- avoid images or videos with large file sizes
- use a single-column layout

Notes:

Try resizing the two wireframed websites from last week to see how responsive they are.

----

## Designing for tablets

How do tablets fit into the picture?

Are tablets mobile devices? Why or why not?

----

## What's next?

----

### The messaging ecosystem

There's a new trend in [messaging-style apps](http://www.adweek.com/news/technology/why-every-marketers-should-be-keeping-evolution-messaging-apps-169998) -- send a text to the app using natural language, and it figures out what you want.

Apple's Siri and Google's Google Now were early versions of this type of service.

What's the benefit of this style of app?

----

Principle

### [Attractiveness bias](https://books.google.ca/books?id=l0QPECGQySYC&lpg=PA20&ots=_O1dZ8vdMs&pg=PA32)

> We tend to see attractive people as more intelligent, competent, moral, and sociable.

----

How might this affect your decision process when creating an experience?

Notes:

konvalwithipad.tumblr.com

----

Principle

### [Aesthetic-usability effect](https://books.google.ca/books?id=l0QPECGQySYC&lpg=PA20&ots=_O1dZ8vdMs&pg=PA20)

> People perceive more aesthetically pleasing designs as more usable, even when they are not.

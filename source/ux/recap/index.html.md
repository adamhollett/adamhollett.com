---
title: Recap
week: 12
class_date: April 5, 2017
theme: night
---

<style>
  .reveal em {
    font-style: inherit;
    background: none;
    color: #ec407a;
  }
</style>

Let's revisit some of the things we've covered so far.

----

## User experience

What is it?

----

> User experience is _all aspects_ of a user's interaction with a company, its services, and its products.

<small>
[Nielsen Norman Group](https://www.nngroup.com/articles/definition-user-experience/)
</small>

----

Studying user experience involves understanding users, gathering data, and acting on feedback to make products and services better and more usable.

----

user experience _≠_ usability

----

## The 5 disciplines

- visual design
- usability
- content strategy
- information architecture
- interaction design

----

### Visual design

Visual design encompasses all the elements you can see that are _not content_.

- <span class="fragment">colours for the background, text, and other elements</span>
- <span class="fragment">layout and positioning of elements on a page</span>
- <span class="fragment">typeface and font size</span>
- <span class="fragment">some illustrations</span>

----

How can visual design affect UX?

- <span class="fragment">colours can evoke different _moods_</span>
- <span class="fragment">layout can make things _easier_ or _harder_ to find</span>
- <span class="fragment">typeface choice can affect _readability_</span>
- <span class="fragment">illustrations can convey _mood or meaning_</span>

----

### Usability

Usability assesses how well an experience performs compared to a user's _expectations_.

- <span class="fragment">does the experience do what it claims to be able to do?</span>
- <span class="fragment">is the product or tool efficient?</span>
- <span class="fragment">is using the product fun or satisfying?</span>

----

How can usability affect UX?

- <span class="fragment">an experience that doesn't do what it says is _frustrating_</span>
- <span class="fragment">a product that takes a long time to learn or use is _annoying_</span>
- <span class="fragment">if using the product doesn't feel good, people will _stop using it_</span>

----

#### Usability principles

We looked at a whole bunch of usability principles during the term.

These are general concepts that you can apply to an experience to understand it better.

----

- <span class="fragment">Occam's razor</span>
- <span class="fragment">contour bias</span>
- <span class="fragment">propositional density</span>

----

### Content strategy

Content strategy is concerned with all the visual and readable **content** in an experience.

- <span class="fragment">the words and phrases used in all written text</span>
- <span class="fragment">the voice and tone used by the experience</span>
- <span class="fragment">the non-textual visual content (like screenshots or photographs) that convey information</span>

<small class="fragment">(video and audio count as content too)</small>

----

How can content affect UX?

- <span class="fragment">simpler language is more readable and accessible for _more users_</span>
- <span class="fragment">the right tone can help users feel _welcome_ or _at ease_</span>
- <span class="fragment">screenshots or photographs can help further explain content</span>

----

### Information architecture

Information architecture is the art of organizing and sorting information to increase usability. This includes:

- <span class="fragment">navigation within the experience</span>
- <span class="fragment">the organization and layout of elements</span>
- <span class="fragment">how things are named</span>

----

How can information architecture affect UX?

- <span class="fragment">sensible, uncomplicated navigation makes things easier to _find_ and _use_</span>
- <span class="fragment">an intuitive layout increases _readability_ and _usability_ and can convey relationships between content</span>
- <span class="fragment">choosing the right name can make things easier to find and read</span>

----

### Interaction design

Interaction design is about designing and implementing the interactive parts of an experience.

----

We didn't study this subject in TWR 2011.

<small>
You probably _shouldn't talk about it_ in your presentations.
</small>

----

## Why we care

Everything applicable to improving UX can apply to creating documentation as well.

Understanding how users experience products and services can helps us write better docs for them.

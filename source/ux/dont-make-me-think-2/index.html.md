---
title: Don't make me think (again)
week: 11
class_date: March 29, 2017
theme: beige
---

Krug explains when it's okay _not_ to use persistent navigation:

----

## On the home page

It's generally okay if the index page of your site looks different from the rest of the site.

See: [Squarespace](https://www.squarespace.com), [Twitter](https://twitter.com/)

----

## Forms

If you're asking a user to submit a form or send information, persistent navigation can take away from the focus of the page.

----

Persistent logos are good, and ought to generally be in the same place.

----

In real life, when you walk in a store, you know you're inside the store until you leave.

On the web, we "teleport" around between different sites, so sometimes it's hard to know when we've left a site.

----

The logo generally belongs in the top left corner -- because this is where people start reading from.

----

Clicking the logo almost always takes you back to the site index.

People expect this, so don't be tempted to make a click on the logo do something else.

----

## Breadcrumbs and information architecture

Sometimes, depending on the website, you can look at the URL to see how the information architecture is structured.

----

Index

`hardwareemporium.biz`

----

Index > Products

`hardwareemporium.biz/products/`

----

Index > Products > Hardware

`hardwareemporium.biz/products/hardware/`

----

The URL can show you what the person building the site named the directories.

I like this, because then your URL essentially acts like free **breadcrumbs**.

----

If you delete everything after `/ux/` on this site, you can get to an index of all the lectures.

Did any of you find that on your own?

----

Sometimes you get URLs that look like this:

`support.emporium.biz/hc/en-us/articles/204403877--problem-with-hammer`

----

This usually indicates that a site is built using a content management system, and URLs are assigned to pages automatically.

----

## Page names

Every page should have a unique name, and the name should be prominent and recognizable.

----

When you click a link labeled "Screwdrivers", you expect to go to a page with the title "Screwdrivers".

----

If you clicked "Screwdrivers" and got to a page called "Hand tools", you'd have to think for a few moments.

----

Worst of all, if you click a link and get a `404 not found` error, you have to think _a lot_.

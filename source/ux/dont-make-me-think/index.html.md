---
title: Don't make me think
week: 10
class_date: March 22, 2017
theme: beige
---

<style>
  button {
    font-size: 1em;
    padding: 0.5em;
  }
</style>

![Book cover of Don't Make Me Think by Steve Krug](/assets/images/ux/dont-make-me-think.png)

**Don't Make Me Think** is a book on website usability by Steve Krug.

This week we'll talk about Krug's "laws" of usability and how they apply to the web.

----

## "Don't make me think"

If there's one usability rule to remember, this is it.

As much as possible, when a user looks at your experience, it should be obvious and self-explanatory.

----

They should be able to "get it" (what it is and how to use it) without thinking about it.

----

What sorts of things make people think?

----

### Jargon

<button class="fragment current-visible">Job-o-rama</button>

<button class="fragment current-visible">Employment opportunities</button>

<button class="fragment current-visible">Jobs</button>

----

### Visual style

<button class="fragment current-visible" style="border: none;">Results</button>

<button class="fragment current-visible" style="background: #7db9e8; border: none; border-radius: 0;">Results</button>

<button class="fragment current-visible" style="background: #1e5799; background: -moz-linear-gradient(top, #7db9e8 0%, #1e5799 100%); background: -webkit-linear-gradient(top, #7db9e8 0%,#1e5799 100%); background: linear-gradient(to bottom, #7db9e8 0%,#1e5799 100%);">Results</button>

----

Every **question** the user has makes the experience harder to use. It all adds to the user's **cognitive load**.

These bumps in the road might be small, but they all add up.

----

What's more, people **don't like it** when they have to take time to figure something out.

----

### Adaptability

What do you type in this box?

![Chrome's address bar](/assets/images/ux/chrome-address-bar.gif)

----

Making pages self-evident and usable is like having better lighting in a store. It just makes everything *seem* better.

It's about developing an effortless experience for the user.

----

## How people really use the web

----

### We don't read everything

People don't *read* websites. They *scan* them.

- We're almost always in a hurry
- We know we don't need to read everything on a site
- We're really good at scanning

----

![What we say to dogs](/assets/images/ux/blah-blah-blah-ginger.jpeg)

----

### We don't make optimal choices

Sometimes designers assume we'll scan the whole page, consider all the options, and choose the one that's best for us.

But that's not the case.

----

Generally, we scan a page and choose **the first reasonable option** that we see.

- Again, we're in a hurry
- There's usually no penalty for guessing wrong
- Guessing is more fun

----

### We don't figure out how things work

People use things all the time without understanding how they work.

**Few people take the time to read instructions**. A lot of people (myself included) would rather just start messing with something and figuring it out.

----

Some people type a website's URL into a search engine *every time they want to go to the site*, not knowing that there's a less complicated way to browse the web.

----

Why do people do this?

- How things "are supposed to work" isn't important to most users (desire lines)
- If we find something that works, we tend to stick to it

----

## Design for scanning

If people are going to scan websites, make websites that are scannable.

----

### Create a clear visual hierarchy

Make more important things more prominent.

----

<p class="fragment" style="font-size: 300%;">Important</p>

<p class="fragment" style="font-size: 200%;">Less important</p>

<p class="fragment" style="font-size: 100%;">Least important</p>

----

or:

<p class="fragment">Important</p>

<p class="fragment" style="opacity: 0.75;">Less important</p>

<p class="fragment" style="opacity: 0.5;">Least important</p>

----

### Group things together

Take a look at [Ottawa Metro](http://www.metronews.ca/ottawa.html), which shows a visual hierarchy as well as content grouping.

Thinking of a site in terms of wireframes can help you visualize content groups.

----

### Use nesting

Sometimes navigation uses nesting to show different levels of hierarchy.

![Nested sidebar hierarchy](/assets/images/ux/nested-sidebar.png)

----

We parse visual hierarchies every day, but it happens so quickly that the only time we're aware that we're doing it is when *we can't do it easily*.

----

### Make clickable things obvious

Users shouldn't have to guess what's clickable and what isn't. Make links and buttons obvious.

Make sure it's obvious when you can click on something.

----

One of my personal pet peeves:

Don't use "click here" for link text.

----

## Encourage mindless choices

Krug says:

> It doesn't matter how many time the user has to click, as long as each click is a mindless, unambiguous choice.

----

## Omit needless words

One of the most useful tips for technical writers is to **use simple language** and **use as few words as possible**.

We've talked about this already.

----

Krug's second law of usability:

> Get rid of half the words on each page, then get rid of half of what's left.

----

The 17th rule in E.B. White's [*The Elements of Style*](http://www.bartleby.com/141/strunk1.html) suggests:

> Vigorous writing is concise. A sentence should contain no unnecessary words, a paragraph no unnecessary sentences, for the same reason that a drawing should have no unnecessary lines and a machine no unnecessary parts.

----

How might you shorten the following sentences?

> Ben is a man who works as a lawyer.

----

> Beatrice sorted the files in an efficient manner.

----

> I accomplished a lot during the course of the day.

----

> That's impressive, if I do say so myself.

----

Here are some other language tools you might like to use (but not rely on):

[grammarly](https://www.grammarly.com)

[Hemingway](http://www.hemingwayapp.com)

----

## Navigation

Krug says:

> People won't use your website if they can't find their way around it.

----

Browsing a website is different from browsing a store:

1.  There's no easy way to know how big a website is, even after you've used it for a long time.

2.  There's no sense of direction -- forward, backward, up, or down.

3.  There's no sense of location. It's hard to figure out how pages relate to one another.

----

When we want to return to something in a store, we remember by thinking about its physical location.

When we want to return to something on a website, we have to remember where it is in the conceptual hierarchy of the site.

----

That's really difficult!

----

We can use **persistent navigation** to help anchor people on a website.

Think about sites you've used and whether the navigational elements on the page change or stay the same as you browse around the site.

----

## Assignment: UI prototype

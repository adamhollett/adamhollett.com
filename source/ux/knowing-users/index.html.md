---
title: Knowing users
week: 2
class_date: "January 18, 2017"
theme: league
---

The purpose of this course is to teach you to _see_ UX.

When we're done, I hope that you will have gained an eye for the good and bad parts of any user experience.

----

<iframe src="https://embed-ssl.ted.com/talks/don_norman_on_design_and_emotion.html" width="640" height="360" frameborder="0" scrolling="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

<small><a href="https://www.ted.com/talks/don_norman_on_design_and_emotion">3 ways good design makes you happy</a> (2003)</small>

----

Design (and UX) can _affect our mindsets_ and _change the way we think and feel_.

The way you feel can affect many other things, such as the way you solve problems.

----

Don Norman wrote a book called **The Design of Everyday Things**.

![](/assets/images/ux/design-of-everyday-things.jpg)

It's a _really good_ and you should read it.

----

To recap, here are the 5 disciplines within UX:

- usability
- content strategy
- visual design
- information architecture
- interaction design

----

## Good UX or bad?

Let's look at a few experiences.

----

![](/assets/images/ux/examples/threadless.jpg)

----

![](/assets/images/ux/examples/phone-lock-during-call.png)

----

![](/assets/images/ux/examples/ruby-developer.png)

----

![](/assets/images/ux/examples/bad-pagination-2.png)

----

## Multichannel experiences

"Multichannel" describes an experience that occurs across more than one "channel" -- television, radio, internet, mobile device, and so on.

----

## Omnichannel experiences

"Omnichannel" describes an experience that uses multiple channels in ways that maximize each channel's unique capabilities.

----

A radio ad for a restaurant could include driving directions or popular routes to the restaurant.

----

Content for mobile users might include scannable barcodes or SMS functionality.

----

A physical kiosk could feature someone demonstrating a product or test products for people to try out.

----

![Omnichannel experience](/assets/images/ux/omnichannel-experience.png)

Example omnichannel experience

----

## Knowing your users

Just like in tech writing, a key part of user experience is **understanding your users**.

----

## Conversion

If you understand where a user in your experience converts from a prospect into a customer, you can design experiences that make that transition easier and more attainable.

----

### Sales funnels

A **sales funnel** visualizes how prospects turn into customers.

It helps determine when users become paying customers, and categorizes users into groups based on their awareness and loyalty.

----

![Sales funnel](/assets/images/ux/sales-funnel.png)

<small>Sales funnel from <a href="http://www.meltwater.com/">Meltwater</a></small>

----

## The balancing act

Good UX can **earn you new users** but it's equally important to think how you can **benefit existing users**.

It's not just your job just to gain new customers or clients. You also have to retain the ones you've already got.

----

## Why good UX matters to you (the creator)

Good, well-crafted user experiences:

- increase customer satisfaction
- increase sales of a product or service
- reduce the need for customer support
- reduce the cost of maintaining or improving products

----

## The bad news about UX

Most users **only notice UX** when it is **bad** or **broken**. When things don't work as they are supposed to, users are more likely to notice and more likely to complain.

----

### Good UX is invisible

When UX designers have done a good job, users are more likely to use an experience, not think about it too much, and go about their day.

They'll be happy, but they probably won't celebrate or tell their friends about it.

----

Can you think of a time you bought or tried a product and were surprised by how much you liked it?

<textarea class="whiteboard" name="good-products" cols="40" rows="5"></textarea>

----

## Why good UX matters to the user

- allows seamless information discovery
- lets users accomplish their goals
- fashions the experience around the user (so they feel important)

----

## Stickiness

An experience is *sticky* if it keeps users engaged or compels them to return frequently.

Several factors contribute to stickiness:

- interesting, meaningful content
- engaging experiences or useful tools
- new information
- interfaces that are easy to use and navigate

----

### Why "users" and not "customers"?

A user isn't always a customer. Good UX benefits existing as well as prospective customers.

Experiences might also be designed for internal use (not visible to the public) or for an organization like a non-profit.

----

The word "user" also helps us focus on the term "use" -- user experience deals with how a person **uses** a product or service, not whether they buy it.

----

Principle

### Affordance

> An object shows **affordance** when it naturally encourages a certain action. For example, a knob *affords* turning, and a chair *affords* sitting.

----

Can you think of any objects that show affordance?

<textarea class="whiteboard" name="affordance" cols="40" rows="5"></textarea>

----

Principle

### 80/20 rule

> In most experiences, 80% of the possible outcomes will come from 20% of the inputs. Users will tend to use only 20% of the functionality 80% of the time.

----

![](/assets/images/ux/microsoft-word-ribbon.png)

Think about the top "ribbon" menu in Microsoft word. How many of these buttons do you use regularly?

----

## Gathering data

Get an understanding of your users by reviewing what you know.

Common sources of user data include:

- market research
- customer segmentation
- user feedback
- sales and customer service teams

----

## User interviews

One of the most fundamental information gathering techniques involves interviewing actual users. These interviews may be *one-on-one* or *focus groups*.

----

## Analyzing user data

Things to pay attention to:

- **behaviours**: What does the user *need*?
- **desires**: What does the user *want*?
- **pain points**: What are the common problems associated with the task? What annoys the user?

----

## Creating user profiles

Creating user **profiles** involves finding meaningful similarities and differences among users.

This lets you cater to more broad user "types" rather than individual users.

----

Sample customer categories for a theater:

- Urban frequent theatergoer
- Tourist
- Occasional theatergoer
- Theater lover

----

What are some possible user categories for a coffee shop?

<textarea class="whiteboard" name="coffee-users" cols="40" rows="5"></textarea>

----

## Prioritize when necessary

Sometimes a particular user group will be more important. Perhaps that group makes up a majority of business, or has the biggest investment in a experience.

It might also be easier to craft a good UX for certain user groups.

----

## Personas

**Personas** are humanized depictions of user profiles. A persona has a name, face, and background.

Personas are fictional archetypes, but they should be created from a real-life understanding of user groups.

----

A persona might contain:

- name
- photo or picture
- quotation
- demographics
- customer segmentation
- key needs/goals
- key pain points
- brand affinities
- technology profile
- general description

----

## Next week

Read ch. 4, **Modeling the Experience** and ch. 5, **Understanding UX as (R)evolution**.

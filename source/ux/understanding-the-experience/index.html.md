---
title: Understanding the experience
week: 3
class_date: "January 26, 2016"
theme: night
transition: fade
---

> More than 50 million personal computers have been sold, and the computer nonetheless remains largely in a world of its own. It is approachable only through complex jargon that has nothing to do with the task for which people use computers.

-- [Mark Weiser](https://en.wikipedia.org/wiki/Mark_Weiser), [*The Computer for the 21st Century*](https://www.ics.uci.edu/~corps/phaseii/Weiser-Computer21stCentury-SciAm.pdf) (1991)

----

## Goals

An experience is always dependent on the user's **end goal**.  Some example user goals:

- apply for a passport
- buy a product online
- research a topic on Google
- create an account on a website
- pay a bill

----

## User scenarios

**User scenarios** are stories that capture a user's end goal and their motivations for using an experience.

![](/assets/images/ux/entering-coffee-shop.jpg)

----

- Who is the user?
- What are their goals?
- What does the user need from the experience?
- Why is the user engaging with the experience?

----

### The user

Find out who the user is by building personas for the experience.

![](/assets/images/ux/personas.jpg)

<small>from <a href="https://www.optimizeroi.com">optimizeroi.com</a></small>

----

Think of the following questions:

- What is the user's gender and age?
- Where do they live and work?
- What is their educational background?
- What is their income bracket?
- What's important to them?
- What makes them unique?

Be as specific, but succinct, as possible.

Notes:

This is a lot like defining a persona. If you have existing personas for an experience, you can use one of those.

----

### The user's goals

Find out what the user is trying to do. Think of the following questions:

- What does the user need to achieve?
- What outcome do they expect?
- What is their budget? (if it's relevant)
- Are there any constraints? (time, travel restrictions, tools)

Focus on the **end goal** here, not the steps in between.

----

### The user's expectations

If you have a persona that corresponds to the user, that's a good place to start.

- What are the motivations behind the user's needs?
- How does they expect to be treated during the experience?
- Are they a novice or an expert?
- Do they need a lot of detail, or just a summary?
- What do they value?
  - convenience?
  - utility?
  - design?
  - multimedia?

----

### The user's motivations

Identify why the user would choose one experience over another.

- What brings them to the experience?
- Why choose this experience over another?
- How important is this destination to the goal? (Maybe this goal is just part of a larger series of goals. How important is it?)

----

Let's come up with scenarios for users in a few situations.

----

If the experience is a **coffee shop**:

<textarea class="whiteboard" name="coffee-shop-scenario" cols="40" rows="5"></textarea>

----

If the experience is a **public library**:

<textarea class="whiteboard" name="public-library-scenario" cols="40" rows="5"></textarea>

----

If the experience is a **corkscrew**:

<textarea class="whiteboard" name="corkscrew-scenario" cols="40" rows="5"></textarea>

----

If the experience is **amazon.com**:

<textarea class="whiteboard" name="amazon-scenario" cols="40" rows="5"></textarea>

----

## Designing user journeys

A **user journey** outlines the processes a user follows to complete a task.

You can find an example user journey on **Page 63** of your textbook.

----

A user journey depends on the user and their individual scenario.

User scenarios describe **the user** and **their motivations**, while user journeys outline **the most logical steps a user takes to achieve their goal**.

User journeys are not meant to include any information about the user.

----

By examining user journeys, you can personalize or modify experiences, or figure out what types of content would suit users.

You can also figure out the processes and tasks that users will have to undertake, and how complicated the tasks are.

----

Let's come up with some user journeys.

----

What steps are required to check your email on your phone?

<textarea class="whiteboard" name="phone-email-journey" cols="40" rows="5"></textarea>

----

What steps are required to order a coffee at a Starbucks?

<textarea class="whiteboard" name="coffee-journey" cols="40" rows="5"></textarea>

----

<textarea class="full whiteboard" name="custom-journey" cols="40" rows="5" placeholder="..."></textarea>

Notes:

Take suggestions and figure out a user journey for something.

----

## Personalization

Personalization delivers specific content to an end user based on a specific context. It considers the following:

- who the user is
- their online behaviour
- where they consume content
- when they consume content
- what they use to access the content
- why they are accessing the content

----

Personalization can make an experience feel like it's tailored for you, but it can also feel intrusive.

----

For example:

A website might detect that you're using an Android device and link to its app on the Google Play Store instead of Apple's App Store.

----

A website might email you a coupon if you add an item to your cart but leave the site before buying.

----

Have you had any personalized experiences?

Do you like personalized experiences? Why or why not?

----

### Different user states for personalization

Using a website as an example:

- *Anonymous* - Nothing is known about the user. You can still learn something about the user by, for example, looking at how they got to the experience (what search term they used).

- *Recognized* - The user has visited before. Perhaps your website has stored a cookie on their computer that tracks their viewing history for your site.

----

- *Known* - The user is known. This might mean that they have registered for an account and have a profile.

- *Influencer* - The user is known and shares the website with others, perhaps through social media.

- *Repeater* - The user is known and returns to the experience often.

----

Much of the data that determines what personalization state a user falls under can come from **analytics**. By looking at the data you've gathered on users, you can figure out how familiar they are with the experience.

----

Think about how these different user states are similar to the sales funnels that we looked at last week.

----

## User journeys and omnichannel

If your experience is **omnichannel**, users will probably move between channels during a typical user journey.

We call it a *touchpoint* when a user interacts with a new channel during a user journey.

----

Principle

### Mental models

> A **mental model** is a user's prediction of how an experience will work based on their past experiences.

----

If your experience surprises the user by behaving in a way that they don't expect, the user is likely to be confused.

----

Principle

### Mapping

> **Mapping** is the relationship between controls, their movements, and their effects on the experience.

Notes:

Examples:

- vehicle steering wheels
- stovetops with multiple burners

----

Turning a steering wheel left makes a car turn left. What if turning the wheel left made the car turn right?

----

When the mappings of controls make sense (that is, they fit our **mental models**), we call them **natural mappings**.

---
title: Staying relevant
week: 9
class_date: March 15, 2017
theme: blood
---

# Staying relevant

----

![Darcy](/assets/images/ux/darcy.jpg)

This is what happens as soon as I decide to make slides for class.

----

To have a successful user experience, you have to **meet user needs**. We know that already.

----

Technology is evolving at an outstanding rate. As more people adopt new technologies, their experiences and expectations change.

----

**Measuring UX** involves understanding UX as a process and a strategy with repeatable steps.

This lets you optimize your process and **iterate** on things that work.

----

![The UX process](/assets/images/ux/ux-process.svg)

----

In [week 1](/ux/defining-ux/#/18) we defined the phases of the UX process, but it's important to think of it as a **cycle** or **loop** that repeats as you gather information and iterate on designs.

----

### Iterating

When the final step is complete, the process kicks off again with the information that has been gained from measurement and evaluation of the whole process.

----

### Measuring

You can measure your functionality, features, and content if you build in testing mechanisms to do so. Determine what is and isn't being used, and study why your users engage with your experience.

----

### Staying relevant

By measuring and iterating, you're ensuring that your experience stays relevant and up-to-date.

----

### Being user-centric

Because you're taking in information about your UX and applying it to make your experience better, your experience is by definition **user-centric**.

----

## Goals, objectives, metrics

To measure results you first have to understand how and what to measure.

----

### Goals

A goal is a long-term achievement. These may or may not be measurable, and are usually more lofty than objectives.

> Be the best commerce platform in the world.

----

### Objectives

An objective is a measurable, quantifiable accomplishment.

> Increase the number of signups by 20%.

> Decrease churn by 10%.

----

Performance-driven, effective UX **always** has defined goals and objectives.

----

But remember that goals and objectives can be **business-oriented** (what's better for business) or **user-oriented** (what's better for the user).

----

Good objectives are **SMART**:

- **S**pecific -- answer the questions *who*, *what*, *when*, *where*, and *why*

- **M**easurable -- the result is quantifiable

- **A**chievable -- the objective must be realistic

- **R**elevant -- the objective must be relevant (what specific users are targeted and what are their goals?)

- **T**ime-specific -- include a time frame to measure progress

----

### Metrics

**Metrics** are how you track your objectives, using things like:

- Analytics
- Key performance indicators (KPIs)
- Conversion

----

Principle

### Propositional density

> A design that conveys multiple meanings through its components is more **dense** and **interesting**.

----

Propositional density it easiest to see in [clever logo designs](http://digitalsynopsis.com/design/50-clever-hidden-meaning-logo-designs), where simple design elements are often used to convey multiple meanings.

----

But you can also apply the concept to experience design.

How many different ways can you make a clickable button convey the action "Go"?

----

Principle

### Iconic representation

> You can use commonly understood *icons* to convey complex actions or concepts.

[Iconic representation in *The Universal Priniples of Design*](https://books.google.ca/books?id=l0QPECGQySYC&lpg=PA2&pg=PA132#v=onepage&q&f=false)

----

There are different categories of iconic representation:

<span class="fragment">
*Similar* icons directly illustrate an action, object, or concept.
For example, a "watch for falling rocks" sign shows a picture of falling rocks.
</span>

<span class="fragment">
*Example* icons use images of things that exemplify or illustrate an action, object, or concept. For example, a fork and knife on a sign for a restaurant.
</span>

----

*Symbolic* icons use images that represent the concept at a higher level of abstraction. For example, a "Lock my computer" button might display an image of a padlock.

<span class="fragment">
*Arbitrary* icons use images that bear little resemblance to the concept they are illustrating. These symbols must be learned. For example, the symbol for radiation.
</span>

----

Principle

### Anthropomorphism

> We tend to see some forms and patterns as being more "humanlike", and are generally attracted to them more.

----

We can use anthropomorphism to attract users to particular design elements or create relationships with users.

----

#### Structural anthropomorphic form

This occurs when we use shapes and arrangements that mimic the human form. A really obvious example would be a picture of a hand pointing to an important link or text input field.

----

#### Gestural anthropomorphic form

This happens when we use movement or poses that mimic the human form. A really good example of this is the user login screen for macOS.

The entire form field shakes back and forth if you enter the wrong password, just like a head shaking "no".

----

#### Character anthropomorphic form

This happens when we display qualities or habits that suggest something human. This could be as simple as using converstional language in your content.

----

Which content is more human?

> Login prevented due to incorrect password.

and

> Hey! It looks like you might have made a typo in your password. We can't log you in until you fix the error.

----

Principle

### Contour bias

> We prefer shapes with curved contours over shapes with sharp edges or points.

----

We can use something this simple to influence our designs as well.

----

Warning signs, which almost always require our attention, are generally angular and sharp-edged.

----

How many of you have a watch with a square face? A round one?

----

Principle

### The uncanny valley

> Anthropomorphic forms are appealing when they are identical to, dissimilar to, or abstract from human forms.
>
> They are distinctly unappealing when the form is very close but not identical to a human being.

----

![](/assets/images/ux/polar-express.jpg)
<small>[The Polar Express (2004)](http://www.imdb.com/title/tt0338348/)</small>

----

Very accurate human forms are good. Abstract human forms are good. Forms that are not-quite-there can be very unsettling.

----

[Free Code Camp](https://www.freecodecamp.com/) (JavaScript)

[The Odin Project](http://www.theodinproject.com/) (Ruby)

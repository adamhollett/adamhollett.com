---
title: Usability testing
week: 8
---

UX and usability testing often go hand-in-hand. Doing a little bit of usability testing can go a long way in improving your UX.

----

Usability is just a part of UX -- so why is this class called "UX and Usability"?

----

UX involves putting the user first when designing experiences. To make sure that an experience is usable, you have to involve the user in the design process, and the experience needs to be tested.

----

Thus, good UX **relies** on usability, perhaps more than its other components -- design, IA, and so on.

----

You've already done a basic form of user testing in the **user and task analysis** assignment.

----

## Common UX testing myths

----

### Myth

> You need a user testing professional.

<span class="fragment">
You can do basic user testing yourself, often with minimal experience or guidance.
</span>

----

### Myth

> User testing requires a full-scale, working prototype.

<span class="fragment">
Nope. Wireframes or even paper models will work.
</span>

----

### Myth

> User testing is the same as usability testing.

<span class="fragment">
Usability testing is the most common type of user testing, but there are other types too.
</span>

----

### Myth

> Usability testing requires a lab environment.

<span class="fragment">
Some testing can even be done remotely, without having the designer and user in the same room.
</span>

----

### Myth

> Effective testing requires a lot of users.

<span class="fragment">
The most common user testing involves small numbers of users giving detailed, qualitative feedback.
</span>

----

### Myth

> Testing is expensive.

<span class="fragment">
User testing **can be** expensive, especially with fancy software and tools -- but simpler approaches to testing can be highly valuable.
</span>

----

### Myth

> User testing slows down development.

<span class="fragment">
User tests often don't take long at all, and can be done in tandem with other things.
</span>

----

### Myth

> You can easily fix problems after launch.

<span class="fragment">
This is partly true, but the costs of "fixing" a problem after it has gone live can be much higher than if it had been fixed earlier.
</span>

----

## Testing methods

There are many kinds of user testing:

- card sorting (building IA)
- tree testing (navigating IA)
- A/B testing
- heat mapping
- accessibility testing

----

## What to test

- visibility of page content
- navigation (primary, secondary, etc.)
- nomenclature (how things are named)
- clarity of content
- common pathways through an experience
- error identification and handling

----

## Prototyping

A prototype is a small-scale mockup that simulates your finished experience.

----

The most common form of prototype is a clickable, on-screen example, but prototypes can be simpler -- illustrations, wireframes, or even sketches.

----

The more detail and effort you put into a prototype, the more accurately it will simulate your final experience.

----

Let's talk about

### simple language

----

The best way to get your message across, especially for technical writers, is to use simple language.

----

> Don't use a five-dollar word when a fifty-cent word will do.
>
> -- Mark Twain

----

Your goal is for your reader to need little effort to parse and understand your content.

----

Let's simplify some phrases.

----

_people often utilize fitness tracking devices to ascertain their level of fitness_

<span class="fragment">
_people use fitness trackers to find out their level of fitness_
</span>

----

_choose your desired option using the **Language** drop-down menu on the top of the page_

<span class="fragment">
_choose an option from the **Language** menu_
</span>

----

_Participants were tested under conditions of good to excellent acoustic isolation_

<span class="fragment">
_We tested the students in a quiet room_
</span>

----

_Decrease the volume levels_

<span class="fragment">
_Lower the volume_
</span>

----

_Comprehension checks were used as exclusion criteria_

<span class="fragment">
_We excluded participants who did not understand the instructions_
</span>

----

_The power on/off switch is red in colour and located at the rear of the device_

<span class="fragment">
_The power switch is red and located on the back of the generator_
</span>

----

## Things to avoid

- <span class="fragment">desire</span>
- <span class="fragment">utilize</span>
- <span class="fragment">functionality</span>
- <span class="fragment">please</span>
- <span class="fragment">very</span>
- <span class="fragment">easy</span>
- <span class="fragment">simply, basically, clearly, obviously</span>
- <span class="fragment">;</span>

----

If you can cut down the length of a sentence and still keep its meaning, give it a shot.

----

This is not to say that you shouldn't use long words ever, rather, that you should err on the side of simpler language most of the time.

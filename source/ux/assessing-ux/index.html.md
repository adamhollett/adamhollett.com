---
title: Assessing UX
week: 4
class_date: February 1, 2016
---

> [It's 2016 already, how are websites still screwing up these user experiences?!](http://www.troyhunt.com/2016/01/its-2016-already-how-are-websites-still.html)

----

## User and task analysis

<small>Due Feb 10, 2016</small>

1. Write a short tutorial for a product, feature, or service.

2. Have a friend or family member follow the tutorial to complete a task.

3. Describe the experience and the user's actions, and analyze how it went.

----

## Blog entry

Any questions on the blog entry due Friday?

----

## WTF does it mean to "design UX"?

Nobody sits down and says "I'm going to design a user experience today".

UX is concerned with the way people use things created by other people.

----

So think about this class not as "learning to create user experiences", rather

_learning to understand the experiences inherent to the things you create_.

You'll probably be creating help content or documentation, but a focus on UX is still helpful.

----

## Taking inventory

If you've already created an experience, or you're working on an existing experience, it can be useful to figure out what you do and do not have.

----

- What can be re-used from the experience? What should be retired?
- Which competitors have similar experiences, and what can you learn from them?
- What guidance can you give for the rest of the design process?
- What technology considerations should inform the experience?

----

Defining the future experience is a critical step to setting up the UX design process.

_What do you want the experience to look like in the future?_

----

## UX is iterative

Websites, apps, and software are continually being improved. This means that an experience is never "finished".

Many (maybe even most) UX projects are redesigns.

----

You can begin iterating by assessing the current experience and asking questions.

----

### What's working?

- What content is being consumed often?
- What positive feedback have you received?
- What features are not matched by any competitors?
- How does the experience help achieve business objectives?


----

### What isn't working?

- What content is not being consumed often?
- Why isn't that content getting the same level of attention?
- Have users complained about anything?
- Where does the experience show usability or design flaws?
- What are the costs of supporting the current experience?

----

### What's missing?

- What do competitors have that you don't?
- Have users requested features or functionality that you don't offer?
- How would a design or UX expert improve the experience?

----

## External assessments

**External assessments** focus on how well the experience supports the needs of its users.

----

## Internal assessments

**Internal assessments** focus on how well the experience supports business objectives.

----

Because both perspectives are important, an assessment usually isn't purely external or purely internal.

----

## The goal

The purpose of an assessment is to decide on the features, requirements, and design guidelines that will make up the revised experience.

----

## Heuristics

The best practices for particular aspects of an experience are called **heuristics**. These are the industry standards that are used to create the most popular experiences.

----

Getting an expert to review your experience against modern best practices is known as **heuristic assessment**.

----

### Common heuristics

----

#### Flexibility

Are there multiple ways to complete tasks?

----

#### Consistency

Is information, content, and functionality presented in a consistent way?

----

#### Predictability

Does the experience behave as a user would expect?

----

#### Error handling

What happens when something goes wrong? Can errors be corrected?

----

#### Simplicity

Is the design easy to understand?

----

#### Guidance

Does the experience provide guidance throughout?

----

#### Clarity of flow

Is there a clear, easy way to complete tasks?

----

Heuristic assessments are most effective when they are conducted by a third-party.

An assessment should be done by someone who has no bias or investment in the product.

----

If you're doing your own heuristic assessment, create a scorecard and rank each of the above heuristics from 1 to 6. See **page 95** in your text for the ranking scale.

----

## Analytics

Analytics tools (like [Google Analytics](https://www.google.ca/analytics/) or [Piwik](https://piwik.org/)) can give you valuable information on how users interact with your web content.

----

Note, however, that analytics can't tell you *why* some content is more popular than other content. Content might be [redundant, outdated, or trivial](https://www.smashingmagazine.com/2015/06/dealing-with-redundant-out-of-date-trivial-rot-content/), or it might just be hard for users to find.

----

Analytics *can* tell you:

- how users navigate through your content
- where the user came from (what site they were on before reaching yours)
- what page they [entered your site on](http://everypageispageone.com/)
- what keywords users searched for before they found your site

----

- if they have visited your site before
- the page where they leave your site
- bounce rate (what percent of visitors leave without browsing)
- how much time users spend on your site

----

## Visual systems assessment

You can assess your experience against visual design standards:

- brand consistency - does your experience visually match brand guidelines?
- clarity and simplicity - is the design uncluttered and easy to digest?
- visual elements - are illustrations and photos legible and high quality?

For reference, here are some of [Twitter's brand guidelines](https://about.twitter.com/company/brand-assets).

----

## Scenario-driven assessment

Using a persona, take a fictional user through the experience, and try to imagine how they might navigate it.

----

### Contextual interviews

Better yet, get an **actual user** to try out the experience. Pay attention, take notes, and ask them to talk through the experience.

This is what you'll be doing in your first assignment.

----

Here are some questions to ask about the contextual interview:

- How long does it take the user to complete the task?

- Is the assumed user journey the same as the actual user journey? If not, how do they differ?

- What prevents the user from completing the task?

- What does the user like and dislike about the experience?

----

## Assessing competitors

You can put yourself in a user's shoes to try out your competitors' products. By doing this, you can perform assessments of the competition.

Here, the goal isn't necessarily to imitate -- you want to have all the info so you can create an experience that differentiates itself.

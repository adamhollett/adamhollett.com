---
title: Defining UX
week: 1
class_date: "January 12, 2016"
theme: night
---

<iframe class="stretch" width="640" height="360" src="https://www.youtube.com/embed/FN_m1Fc0YmM?rel=0&amp;showinfo=0" style="border: none" allowfullscreen></iframe>

<small>from **The Gods Must be Crazy**, 1980</small>

----

## Defining user experience

UX describes how a **user** (someone with a goal in mind) interacts with an **experience** (a product, device, or interface).

----

If a product, tool, or website is easy and enjoyable to use, it's probably because someone has considered the user experience.

By studying what makes experiences effective and usable, we can learn how to create better experiences.

----

## Technical writing and UX

UX plays a huge part in understanding how to help people more effectively.

Knowing the strengths and flaws of an experience will help you to explain it better when creating help material or documentation.

----

## Components of UX

User experience includes all of the following:

- information architecture
- content strategy
- interaction design
- usability
- visual design

----

## UX is **information architecture**

Information architecture is *the way information is organized*. This includes:

- navigation
- content organization
- visual arrangement
- interaction design

----

## UX is **content strategy**

Content strategy determines *all the content that is presented to a user*. "Content" means:

- text
- images
- video
- audio

----

## UX is **interaction design**

Interaction design is *the way a user interacts* or *can interact* with something.

- what happens when users navigate, click buttons, or follow links?
- what interactions are needed to let the user accomplish their goals?
- what interactions are possible?

----

## UX is **usability**

Usability determines how well an experience performs compared to a user's expectations or intentions.

- does the experience do what it claims to do?
- are goals easy to accomplish?
- is the experience clear and intuitive?

![The 5 Es](/assets/images/ux/5-es-of-use.png)

<small>Whitney Quesenbery's 5Es (<http://wqusability.com/articles/getting-started.html>)</small>

----

## UX is **visual design**

Anything visible in an experience makes up the visual design. Visual design:

- makes the experience pleasing to use
- ensures that the experience follows brand guidelines
- helps a user interpret and use content

----

Principle

### Occam's razor

> Given a choice between functionally equivalent designs, the simplest design is the better choice.

----

Principle

### Flexibility / usability tradeoff

> As the flexibility of an experience increases, the usability of the experience decreases.

----

A pencil has **basic** functionality but **high** usability.

![Pencil](/assets/images/ux/pencil.png)

----

A television remote has **more** functionality but inherently **low** usability.

![Remote](/assets/images/ux/remote.png)

----

Usability is *a part* of user experience, but user experience is a much broader field that focuses on usefulness and overall user engagement with an experience.

![User experience honeycomb](/assets/images/ux/user-experience-honeycomb.png)
<small>Peter Merville's user experience honeycomb (<http://semanticstudios.com/user_experience_design>)</small>

----

### Usability

Answers the question "How easy is an experience to use?"

### User experience

Answers the questions "How useful was the experience?" and "How enjoyable was the experience to use?"

----

## UX inputs

When designing an experience, you can draw on a number of information sources or **inputs**.

That is, if we have to make something for people to use, we can draw on resources to help shape a better UX:

- user profiles or personas
- user scenarios and user journeys
- brand guidelines
- experience models

----

## UX considerations

- understand your target users
- decide whether to start fresh or redesign
- identify your technologies
- post-launch maintenance
- ensure consistency
- determine your level of comfort

----

## Project phases

1.  discover/define
2.  design
3.  build
4.  test and launch
5.  maintain

(repeat)

----

## Summary

- UX is a multi-faceted domain
- usability is just one part of UX
- many different inputs make up a user experience
- UX can be involved in all phases of a project

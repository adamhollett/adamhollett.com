---
title: Introduction
week: 0
theme: night
---

## Hi.

I'm Adam Hollett.

----

I'm from Pasadena, Newfoundland.

![Map of Newfoundland with an X on the town of Pasadena](/assets/images/ux/newfoundland-map-pasadena.png)

----

I've worked at [Solace Systems](https://solace.com) and [Shopify](https://www.shopify.com). I like front-end development (web design), automation, and accessibility.

----

At Shopify I take care of the tools, development, and build process for the help website that we maintain. I also maintain documentation for the open-source template language [Liquid](https://github.com/Shopify/liquid).

I do a lot of work with static site generators.

----

I'm kind of crazy about apps.

I'm always on the lookout for new tools and apps that will help me get things done. I'm always trying out alternatives and figuring out which apps work best for me.

----

But what makes an app "work best for you"?

----

In this course we'll talk about **user experience**, which looks at the way people interact with and use things.

By learning how people respond to experiences, we can learn how to build and document experiences better.

----

The textbook for this course is **UX for Dummies**, by D. Chesnut and K. P. Nichols.

![UX for Dummies](/assets/images/ux/ux-for-dummies.jpg)

----

If you need help, you can email me:

<holleta@algonquincollege.com>

----

I'll try to post links to class slides shortly after each class.

----

Course info and assignments are on [Blackboard](https://blackboard.algonquincollege.com/webapps/blackboard/execute/launcher?type=Course&id=_349998_1).

----

## Evaluation

Your grade will come from the following:

- 4 blog entries (5% each)
- User and task analysis (20%)
- Collaborative wiki article (20%)
- UI protoype (20%)
- Website presentation (20%)

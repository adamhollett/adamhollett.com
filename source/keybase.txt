==================================================================
https://keybase.io/admhlt
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://adamhollett.com
  * I am admhlt (https://keybase.io/admhlt) on keybase.
  * I have a public key ASAGMg-cA88zA_1XrmU45LsEbPcdld_v0ZHmjp2cCK-mPAo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120888238651442c9ef340d231a6b7a2e1e4c2f1a72852a705df3274d5f870736000a",
      "host": "keybase.io",
      "kid": "012006320f9c03cf3303fd57ae6538e4bb046cf71d95dfefd191e68e9d9c08afa63c0a",
      "uid": "d6dc899d320f393edf3f06810b684319",
      "username": "admhlt"
    },
    "merkle_root": {
      "ctime": 1554906375,
      "hash": "a2c4365b43865207e8df3de45390155c0055a43f4a5009fe6aab298878fccf3e120ae9c7bcd9ac9b0078898e064ea5bdceaa033ccbc1d84cb2bb9a7ad929e56d",
      "hash_meta": "5bcd95888df1bc2cd43e0987518d0b5e49507f424e7eebf5c02ecbb3620250eb",
      "seqno": 5134929
    },
    "service": {
      "entropy": "j/PicEIMa7Wati38HmOlQxPt",
      "hostname": "adamhollett.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "3.1.2"
  },
  "ctime": 1554906427,
  "expire_in": 504576000,
  "prev": "dccf1f426849594b438d712fb9eb8bf5e17f18c863444657ffb1354978460678",
  "seqno": 20,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgBjIPnAPPMwP9V65lOOS7BGz3HZXf79GR5o6dnAivpjwKp3BheWxvYWTESpcCFMQg3M8fQmhJWUtDjXEvueuL9eF/GMhjREZX/7E1SXhGBnjEIOQwRse6CeXSWpVX6JOwtnKvLu43UOeedrhUs82+LqxMAgHCo3NpZ8RAasxNcshWEd5sOciQI5D7psddX7/+0Dj/Ha46sotlbb5J9O14F5nfrFk9rwGSzLu77kmCjxcmzN3B0PiYDz3GCKhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIPpWqAAdvUsaKl+EY1loYubnChy3RbZ7Zsf0EZihsA2Mo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/admhlt

==================================================================

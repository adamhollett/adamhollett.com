# frozen_string_literal: true

require 'slim'

## General configuration

activate :directory_indexes
activate :sprockets

## Layouts

### Page layout
page 'posts/*.html',      layout: :post
page 'sandbox/**/*.html', layout: :blank
page 'slides/*',          layout: :slides
page 'ux/*.html',         layout: :slides
page '/*.html',           layout: :page

### No layout
page '/*.xml',            layout: false
page '/*.json',           layout: false
page '/*.txt',            layout: false

Slim::Engine.set_options(
  pretty:     true,
  sort_attrs: false
)

## Blogging
activate :blog do |blog|
  blog.prefix = 'posts'
  blog.sources = '{year}/{month}-{day}-{title}.html'
  blog.permalink = '{year}/{month}/{title}.html'
end

## Syntax highlighting
activate :syntax

## Assets
config[:css_dir]    = 'assets/stylesheets'
config[:images_dir] = 'assets/images'
config[:js_dir]     = 'assets/javascripts'

## Development settings
configure :development do
  activate :livereload # Refresh the browser when pages change
end

# Build settings
configure :build do
  set :build_dir, 'public'
  activate :minify_css        # Minify CSS on build
  activate :minify_javascript # Minify JavaScript on build
end

# Generate shortlinks
data.shortlinks.each do |link|
  proxy "/#{link[0]}/index.html", '/shortlink.html', layout: false, locals: { destination: link[1] }, ignore: true
end
